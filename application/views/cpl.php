<div class="content">
    <div class="container_12">
        <div class="grid_12">
            <h3 class="pb1">
                <span><div class="sbfml">CONCORD PHARMACEUTICALS LTD.</div></span>
            </h3>      
            <div class="sbfml_content">
                <div class="sbfml_menu">
                    <ul>
                        <li><a href="<?php echo base_url();?>welcome/cpl.aspx">Overview</a></li>
                        <li><a href="<?php echo base_url();?>welcome/product_list.aspx">Product List</a></li>
                    </ul>
                </div>
                <div class="sbfml_right_content">
                    <div style="width:60%;float:left; color:#000;">
                        <h4>Overview</h4>
                        <p>CONCORD Pharmaceuticals Ltd. the fast growing company is committed to manufacturing of quality medicines                  through strict adherence to internationally accepted standards of Good Manufacturing Practice. </p>
                        <h4>Vision</h4>
                        <p>CONCORD Pharmaceuticals Ltd. wants to become a research based global pharmaceutical company with a highly                efficient generic manufacturer. Through the State of Art Technology, we want to provide the nation with high                quality health care products and discover the innovation through different researches (e.g. Bioequivalent                   Test) in the Pharmaceutical sector.</p>
                        <h4>Quality Policy</h4>
                        <p>We are determined to develop and establish the image of innovative & value-added products that improves                   the quality of life of people across the country & around the world.We belief in "Better Medicine Better                    life" which contributes towards the growth of a healthy Nation.</p><br>
                        <h4>Factory</h4>
                        <p>The factory of Concord Pharmaceuticals Ltd. (CPL) is situated at Kanchpur, BSCIC in Narayangonj district,
                            15 km far from Dhaka city. The company produces various types of drugs, which include tablets, capsules,
                            oral liquids, capsules etc. </p>
                    </div>
                    <div style="width: 36%; float:right;">
                        <div class="port">
                            <div class="grid_4">
                                <a href="images/cpl/4.jpg" class="gal"><img src="<?php echo base_url();?>images/cpl/4.jpg" class="imgimg" alt="" /></a>
                            </div>

                            <div class="grid_4">
                                <a href="images/cpl/3.jpg" class="gal"><img src="<?php echo base_url();?>images/cpl/3.jpg" class="imgimg" alt="" /></a>
                            </div>
                        </div>
                    </div>
                    <div style="clear: both;"></div>
                    <h4>Production Facilities</h4>
                    <p>The building contains five floors of 15,500 sq feet. The first 3 floors are being used for production and
                        the fourth floor is for Admin, QC and conference cum training room. The fifth floor is being used for
                        Purified
                        water plant, Maintenance Section, and Effluent Treatment Plant (ETP).Each room is equipped with A/C. in
                        order
                        to ensure sufficient fresh air in the production zone as well as in the corridors, critical rooms are also
                        equipped with exhausted fan.  The humidity of each production room is strictly maintained with dehumidifie
                        .</p>

                    <div style="width:60%;float:left; color:#000;">
                        <h4>Design of the room and air conditioning system ensures</h4>
                        <ul>
                            <ol>i) Prevention of contamination of the products</ol>
                            <ol>ii) Protection of the environment</ol>
                            <ol>iii) Protection of people</ol>
                            <ol>iv) Prevention of insects, birds, rats etc.</ol>
                        </ul>
                    </div>
                    <div style="width: 36%; float:right;">
                        <div class="port">
                            <div class="grid_4">
                                <a href="<?php echo base_url();?>images/cpl/1.jpg" class="gal"><img src="images/cpl/1.jpg" class="imgimg" alt="" /></a>
                            </div>
                        </div>
                    </div>
                    <div style="clear: both;"></div>
                    <h4>Product Marketing</h4>
                    <p>Pharmaceutical product Marketing is an indirect marketing where the Medical Representatives promote and
                        convince about company product to Doctors. Doctor is the decision maker to prescribe the product and patients
                        takes it from retail shop or chemist.</p>
                    <img src="<?php echo base_url();?>images/cpl/5.png" style="width: 100%; height:150px; margin-bottom:15px;"/>
                    <h4>The company directly sell product to-</h4>
                    <ul>
                        <ol>i) Wholesaler</ol>
                        <ol>ii) Hospital</ol>
                        <ol>iii) Retailer/Chemist</ol>
                    </ul><br>
                    <h4>Distribution</h4>
                    <p>Nationally the company distributes its products through ten distribution points or depots.</p>

                </div>
            </div>    
        </div>
    </div>
</div>
