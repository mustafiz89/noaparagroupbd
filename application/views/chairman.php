<div class="content">
  <div class="container_12">
    <div class="grid_12">
      <h5 align="center">Bismillahir Rahmanir Rahim</h5>
      <div class="chairman_pic">
        <img src="<?php echo base_url().$chairman_message->image;?>"/>
      </div>
      <div class="p">
          <p><?php echo $chairman_message->message_list; ?></p>
        <br><br>
        <div class="style">
            <span class="col2 upp"><?php echo $chairman_message->chairman_name?></span><br>
            <span class="col2 upp">Chairman</span><br>
            <span class="col2 upp">Noapara Group</span>
         </div>
      
     </div>
    <div class="clear"></div>
    
  </div>
</div>
</div>