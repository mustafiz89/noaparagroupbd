
<div class="content">
    <div class="container_12">
        <div class="grid_12">
            <h3 class="pb1">
                <span><div class="sbfml">South Bengal Fertilizer Mills Ltd.</div></span>
            </h3>
            

            <div class="sbfml_content">
                <div class="sbfml_menu">
                    <ul>
                        <li><a href="<?php echo base_url();?>welcome/organ.aspx">Organizational Organogram</a></li>
                        <li><a href="<?php echo base_url();?>welcome/product_marketing.aspx">Product Marketing</a></li>
                        <li><a href="<?php echo base_url();?>welcome/Man_machine.aspx">The Man Behilnd The Machine</a></li>
                        <li><a href="<?php echo base_url();?>welcome/quality_assurance.aspx">Quality Assurance</a></li>
                        <li><a href="<?php echo base_url();?>welcome/product_info.aspx">Product Information</a></li>
                        <li><a href="<?php echo base_url();?>welcome/photo_gallery.aspx">Photo Gallary</a></li>
                    </ul>
                </div>
                <div class="sbfml_right_content">
                    <p>South Bengal Fertilizer mills Ltd, a sister concern of the group is established as a private Company limited              by shares, located at Mohakal, Noapara, Avoynagar, Jessore, Bangladesh. The Compony produces NPKS of different              grades as approved government as well as per market demand by applying modern technology. The Company has also              installed Packaging plant to pack Magnesium sulphate and Zinc Sulphate in 1 (one) Kg Bag, with a capacity of                100 M.ton Per day. Key information of the Factory are as foIlows:</p>
                    <div style="width:430px;float:left;">
                        <p><span class="span">A. Grade available as per approval of Government</span></p>
                        <p>
                            1. For Paddy (Local Variety)	:	i) &-20-14-5</p>
                        <p>Hybrid Variety)	:	ii) 10-24-17-6</p>
                        <p>2. ForRobi crops (Vegetables)	:	i)12-15-20.6</p>
                        3. Size of Product	:	(2-4) mm> 90%
                        4. Power Supply	:	REB with Stand by Generator<br>
                        <span class="span" style="margin-right:5px;">B. No of Plant</span>  2 (Two)<br>
                        <span class="span" style="margin-right:5px;">C. Plant Capacity </span> i) Daily: 200x2 = 400 M.Ton
                        ii) Yearly: 120,000 M,Ton<br>
                        <span class="span" style="margin-right:5px;">D. Plant Area:</span> 400 Decimal<br>
                        <span class="span" style="margin-right:5px;">E. Machine Origin:</span> The People's Republic of China<br>
                        <span class="span" style="margin-right:5px;">F. Date of Commercial Production:</span> September 2003<br><br>

                        <h4>Other information:</h4>
                        <p>Company achieves ISO 9001:2000through maintaining Quality Management System in all</p> 

                        <h4>Quality Policy of the Plant:</h4>
                        <p>We set our goals to achieve customer satisfaction through manufacturing high quality mixed fertilizer and                    providing it with competitive price & on time delivery. South Bengal Fertilizer Mills Ltd is committed to                   achieve this by providing adequate resources, trained manpower, utilizing latest technology and continual                   improvement of ISO 9001:2000 QMS processes.</p>
                    </div>
                    <div style="width:220px; float:right;">
                        <div class="port">
                            <div class="grid_3">
                                <a href="images/SouthBengal.jpg" class="gal"><img src="<?php echo base_url(); ?>images/SouthBengal.jpg" style="width:220px; height:250px;"/></a>
                            </div>
                        </div>

                    </div>
                    <div class="clear"></div>
                </div>
            </div>


            <div class="grid_12">
                <h3 class="pb1">
                    <span><div class="view">DIFFERENT VISIT VIEW OF SOUTH BENGAL FERTILIZER MILLS LTD.</div></span>
                </h3>
            </div>
            <div class="visit_view">
                <div class="port">
                    <div class="grid_4">
                        <a href="images/visit/1.jpg" class="gal"><img src="<?php echo base_url(); ?>images/visit/1.jpg" alt="" class="viewimg"></a>
                    </div>

                    <div class="grid_4">
                        <a href="images/visit/2.jpg" class="gal"><img src="<?php echo base_url(); ?>images/visit/2.jpg" alt="" class="viewimg"></a>
                    </div>

                    <div class="grid_4">
                        <a href="images/visit/3.jpg" class="gal"><img src="<?php echo base_url(); ?>images/visit/3.jpg" alt="" class="viewimg"></a>
                    </div>
                </div>
            </div>    

        </div>
        <div class="clear"></div>

    </div>
</div>
