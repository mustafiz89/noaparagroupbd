<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $title;?></title>
        <meta charset="utf-8">
<!--        <link rel="icon" href="<?php echo base_url(); ?>images/favicon.ico">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>images/favicon.ico"/>-->
        <link rel="shortcut icon" href="<?php echo base_url();?>img/favicon.ico">
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/style.css">
        <!-- Main Menu-->
        <link href="<?php echo base_url(); ?>menu.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.7.2.min.js"></script>
        <script type='text/javascript' src='<?php echo base_url(); ?>js/jquery.hoverIntent.minified.js'></script>
        <script type='text/javascript' src='<?php echo base_url(); ?>js/jquery.dcmegamenu.1.2.js'></script>
        <script type="text/javascript">
            $(document).ready(function($) {
                $('#mega-menu-tut').dcMegaMenu({
                    rowItems: '3',
                    speed: 'fast'
                });
            });
        </script>

        <!-- Main Menu-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/touchTouch.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/camera.css">
        <script src="<?php echo base_url(); ?>js/jquery2.js"></script>
        <script src="<?php echo base_url(); ?>js/jquery-migrate-1.1.1.js"></script>
        <script src="<?php echo base_url(); ?>js/superfish.js"></script>
        <script src="<?php echo base_url(); ?>js/touchTouch.jquery.js"></script>
        <script src="<?php echo base_url(); ?>js/jquery.equalheights.js"></script>
        <script src="<?php echo base_url(); ?>js/jquery.easing.1.3.js"></script>
        <script src="<?php echo base_url(); ?>js/camera.js"></script>
        <!--[if (gt IE 9)|!(IE)]><!-->
        <script src="<?php echo base_url(); ?>js/jquery.mobile.customized.min.js"></script>
        <!--<![endif]-->
        <script>
            $(document).ready(function() {
                jQuery('#camera_wrap').camera({
                    loader: false,
                    pagination: false,
                    thumbnails: false,
                    height: '32.92857142857143%',
                    minHeight: '300',
                    caption: false,
                    navigation: true,
                    fx: 'mosaic'
                });
            });

        </script>
        <script>
            $(function() {
                // Initialize the gallery
                $('.port a.gal').touchTouch();
            });
        </script>
        <!--[if lt IE 8]>
          <div style=' clear: both; text-align:center; position: relative;'>
            <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
              <img src="http://storage.ie6countdown.com/assets/100/<?php echo base_url(); ?>images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
            </a>
         </div>
       <![endif]-->
        <!--[if lt IE 9]>
          <script src="<?php echo base_url(); ?>js/html5shiv.js"></script>
          <link rel="stylesheet" media="screen" href="<?php echo base_url(); ?>css/ie.css">
    
        <![endif]-->
    </head>

    <body>
        <div id="header">
            <div id="logo">
                <a href=""><img src="<?php echo base_url(); ?>images/logo/1.jpg"></a>
            </div>
            <div id="mainmenu">
                <div class="dcjq-mega-menu">
                    <ul id="mega-menu-tut" class="menu">
                        <li><a href="<?php echo base_url();?>welcome/index.aspx">Home</a></li>

                        <li><a href="<?php echo base_url();?>welcome/profile.aspx">Profile</a></li>
                        <li><a href="#">Sister Concerns</a>
                            <ul>
                                <li id="menu-item"><a href="<?php echo base_url();?>welcome/sbfml.aspx">South Bengal Fertilizer Mills Ltd.</a></li>


                                <li id="menu-item"><a href="<?php echo base_url();?>welcome/ncml.aspx">Noapara Cement Mills Ltd.</a> </li>
                                <li id="menu-item"><a href="<?php echo base_url();?>welcome/cpl.aspx">Concord Pharmaceuticals Ltd.</a> </li>
                            </ul>
                        </li>
                        <li><a href="<?php echo base_url();?>welcome/visualization.aspx">Visualization</a></li>
                        <li><a href="<?php echo base_url();?>welcome/contact_us.aspx">Contact us</a></li>
                    </ul>
                </div> 
            </div>

        </div>
        <div class="clear"></div>
        <!-- ALL Content-->
        <body  class="page1">
        <?php 
        echo $mid_content;
        ?>
            </body>
<!---footer-->
        <div id="footer">  
            <div class="container_12">
                <div class="grid_12">
                    <div class="socials">
                        <a href="#"></a>
                        <a href="#"></a>
                        <a href="#"></a>
                        <a href="#"></a>
                    </div>
                    <div class="copy">
                        WebDesign (C) 2013 | <a href="#">Privacy Policy</a> | Website designed & developed by <a target="_blank" href="http://www.dynamicsoftwareltd.com/" rel="nofollow">Dynamic Software Limited</a>
                    </div>
                </div>
            </div>
        </div> 

        
    </body>	 
</html>