<div class="content">
    <div class="container_12">
        <div class="grid_12">
            <h3 class="pb1">
                <span><div class="sbfml">ORGANIZATIONAL ORGANOGRAM</div></span>
            </h3>

            <div class="sbfml_content">
                <div class="sbfml_menu">
                    <ul>
                        <li><a href="<?php echo base_url(); ?>welcome/organ.aspx">Organizational Organogram</a></li>
                        <li><a href="<?php echo base_url(); ?>welcome/product_marketing.aspx">Product Marketing</a></li>
                        <li><a href="<?php echo base_url(); ?>welcome/Man_machine.aspx">The Man Behilnd The Machine</a></li>
                        <li><a href="<?php echo base_url(); ?>welcome/quality_assurance.aspx">Quality Assurance</a></li>
                        <li><a href="<?php echo base_url(); ?>welcome/product_info.aspx">Product Information</a></li>
                        <li><a href="<?php echo base_url(); ?>welcome/photo_gallery.aspx">Photo Gallary</a></li>
                    </ul>
                </div>
                <div class="sbfml_right_content">
                    <img src="<?php echo base_url(); ?>images/organ.png" style="width:100%;"/>          
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
</div>
