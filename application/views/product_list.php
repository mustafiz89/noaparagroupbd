<div class="content">
  <div class="container_12">
    <div class="grid_12">
      <h3 class="pb1">
      <span><div class="grouppro">PRODUCT LIST</div></span>
      </h3>
      <div class="extra_wrapper" style="width: 100%;">
        <table class="myOtherTable">
            <tr><th style="width:10%;">SL NO</th><th style="width: 30%">Name of Products</th><th style="width:50%">Generic Name</th></tr>
            <?php 
            $i=0;
            foreach($all_product as $v_product)
            {
                $i+=1;
            ?>
            <tr>
                <td><?php echo $i; ?></td>
                <td><?php echo $v_product->product_name;?></td>
                <td><?php echo $v_product->product_generic_name;?></td>
            </tr>
            <?php
            }
            ?>
        </table>
      
      </div>
    </div>
    <div class="clear"></div>
    
  </div>
</div>