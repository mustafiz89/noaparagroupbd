<div class="content">
  <div class="container_12">
    <div class="grid_12">
      <h3><span>Contact Us</span></h3></div>
      <div class="grid_5">
        <div class="map">
            <figure class="">
                <div>
                    <iframe width="400" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" 
                    src="http://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=Link%2BIT%2B144%2BMotijheel%2C%2BDhaka-1000%2C%2BBangladesh(Dynamic+Software+Limited)&ie=UTF8&z=15&t=m&iwloc=near&output=embed"></iframe><br><table width="400" cellpadding="0" cellspacing="0" border="0"></table>
                </div>
            </figure>
            <contact>
               <div style="color: #000;">
               <h3><span>Office Location</span></h3>
                
              
                <div class="office">
                    <h4>Corporate Office</h4>
                        <ul>
                            <li>
                                Noapara Bazar, Noapara, Jessore, Bangladesh
                            </li>
                            <li>
                                Phone: 04222-7121771442
                            </li>
                            <li>
                                Mobile: +8801711295152,+8801713-480777
                            </li>
                            <li>
                                Fax: 04222-71299
                            </li>
                            <li>
                                E-mail: <span class="col1"><a href="mailto:noapara_trd@yahoo.com">noapara_trd@yahoo.com</a></span>
                            </li>                            
                        </ul>
                </div>
               <br>
               <div class="office">
                    <h4>Dhaka Office</h4>
                        <ul>
                            <li>
                                Baitul Hossain Buliding(1st Floor) 27, Dilkusha C/A,Dhaka-1000, Bangladesh
                            </li>
                            <li>
                                Phone: 02-7117449,7117551,9578820,9578853
                            </li>
                            <li>
                                Fax: +88-02-7115698
                            </li>
                                E-mail: <span class="col1"><a href="mailto:noapara_trd@yahoo.com">noapara_trd@yahoo.com</a></span>
                            </li>                            
                        </ul>
                </div>
                </div>     
            </contact>
        </div>              
      </div>
      <div class="grid_6 prefix_1">
          
          <div style="color:green; font-size: 20px">
            <?php
             $msg=$this->session->userdata('message');
             if($msg)
             {
                 echo $msg;
                 $this->session->unset_userdata('message');
             }
            ?>
            </div>
          
          
          
          <form id="form" style="color: #000;" action="<?php echo base_url();?>welcome/send_mail" method="post
                
                ">
              <div class="success_wrapper">
                  <div class="success"><br>
                      <strong></strong> </div></div>
              <fieldset>
                  <label class="name">
                      <input type="text" name="name"placeholder="Name:" value="">
                      <br class="clear">
                      <span class="error error-empty"></span><span class="empty error-empty"></span></label>
                  <label class="phone">
                      <input type="tel" name="mobile"placeholder="Mobile:" value="">
                      <br class="clear">
                      <span class="error error-empty"></span><span class="empty error-empty"></span></label>
                  <label class="email">
                      <input type="text" name="email_address"placeholder="E-mail:" value="">
                      <br class="clear">
                      <span class="error error-empty"></span><span class="empty error-empty"></span></label>

                  <label class="message">
                      <textarea name="message"> </textarea>
                      <br class="clear">
                      <span class="error"></span> <span class="empty"></span> </label>
                  <div class="clear"></div>
                  <div class="btns">

                      <input type="submit" class="btns" value="Send"/>

                  </div>
              </fieldset>
          </form>
          
        For any Support: <br>
  E-mail: <span class="col1"><a href="#">info@noaparagroupbd.com</a></span> <br>
  Phone: +88 02-7117551, +88 02-7117449
      
    </div>
  </div>
</div>