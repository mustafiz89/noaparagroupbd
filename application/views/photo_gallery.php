<div class="content">
    <div class="container_12">
        <div class="grid_12">
            <h3 class="pb1">
                <span><div class="sbfml">SOUTH BENGAL'S PHOTO GALLARY</div></span>
            </h3>
            <img src="<?php echo base_url(); ?>images/page4_img1.jpg" alt="" class="img_inner fleft">

            <div class="sbfml_content">
                <div class="sbfml_menu">
                    <ul>
                        <li><a href="<?php echo base_url(); ?>welcome/organ.aspx">Organizational Organogram</a></li>
                        <li><a href="<?php echo base_url(); ?>welcome/product_marketing.aspx">Product Marketing</a></li>
                        <li><a href="<?php echo base_url(); ?>welcome/Man_machine.aspx">The Man Behilnd The Machine</a></li>
                        <li><a href="<?php echo base_url(); ?>welcome/quality_assurance.aspx">Quality Assurance</a></li>
                        <li><a href="<?php echo base_url(); ?>welcome/product_info.aspx">Product Information</a></li>
                        <li><a href="<?php echo base_url(); ?>welcome/photo_gallery.aspx">Photo Gallary</a></li>
                    </ul>
                </div>
                <div class="sbfml_right_content margin">
                    <div class="clear"></div>
                    <div class="port">
                        
                       <?php 
                       foreach ($all_photo as $v_photo)
                       {
                       
                       ?>
                        <div class="grid_2">
                            <a href="<?php echo base_url().$v_photo->image;?>" class="gal"><img src="<?php echo base_url().$v_photo->image;?>" alt=""></a>
                        </div>
                        
                        <?php
                       }
                       ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
