<div class="content">
    <div class="container_12">
        <div class="grid_12">
            <h3 class="pb1">
                <span><div class="sbfml">QUALITY ASSURANCE</div></span>
            </h3>
            <img src="<?php echo base_url();?>images/page4_img1.jpg" alt="" class="img_inner fleft">

            <div class="sbfml_content">
                <div class="sbfml_menu">
                    <ul>
                         <li><a href="<?php echo base_url();?>welcome/organ.aspx">Organizational Organogram</a></li>
                        <li><a href="<?php echo base_url();?>welcome/product_marketing.aspx">Product Marketing</a></li>
                        <li><a href="<?php echo base_url();?>welcome/Man_machine.aspx">The Man Behilnd The Machine</a></li>
                        <li><a href="<?php echo base_url();?>welcome/quality_assurance.aspx">Quality Assurance</a></li>
                        <li><a href="<?php echo base_url();?>welcome/product_info.aspx">Product Information</a></li>
                        <li><a href="<?php echo base_url();?>welcome/photo_gallery.aspx">Photo Gallary</a></li>
                    </ul>
                </div>
                <div class="sbfml_right_content">
                    <p>The quality of Bengal NPKS is maintained at highest level and test the item through spectrophotometer 
                        & Flame Photometer.</p>    

                    <div class="port">
                        <div class="grid_4">
                            <a href="<?php echo base_url();?>images/qa/qa1.jpg" class="gal"><img src="<?php echo base_url();?>images/qa/qa1.jpg" class="qaimg" alt="" /></a>
                        </div>

                        <div class="grid_4">
                            <a href="<?php echo base_url();?>images/qa/qa2.jpg" class="gal"><img src="<?php echo base_url();?>images/qa/qa2.jpg" class="qaimg" alt="" /></a>
                        </div>

                        <div class="grid_4">
                            <a href="<?php echo base_url();?>images/qa/qa3.jpg" class="gal"><img src="<?php echo base_url();?>images/qa/qa3.jpg" class="qaimg" alt="" /></a>
                        </div>

                        <div class="grid_4">
                            <a href="<?php echo base_url();?>images/qa/qa4.jpg" class="gal"><img src="<?php echo base_url();?>images/qa/qa4.jpg" class="qaimg" alt="" /></a>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
</div>