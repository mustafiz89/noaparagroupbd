<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i>Add Pharmacy Product</h2>
            <div class="box-icon">
                <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>
        
        <div class="box-content">
            <div style="color:green; font-size: 16px;">
            <?php 
             $msg=$this->session->userdata('message');
             if($msg)
             {
               echo $msg;
               $this->session->unset_userdata('message');
             }
            ?>
            
            </div>
            
            <form class="form-horizontal" action="<?php echo base_url(); ?>administrator/save_pharmacy_product" method="post" enctype="multipart/form-data">
                <fieldset>
                    <legend>
                        
                    </legend>
                    <div class="control-group">
                        <label class="control-label" for="textarea2">Product Name(<span class="required">*</span>)</label>
                        <div class="controls">
                            <textarea class="" name="product_name" id="textarea2" required rows="3" style="width:500px; height:40px;" maxlength="250"><?php //echo $message_info->message_short_list;?></textarea>
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label" for="textarea2">Product Generic Name(<span class="required">*</span>)</label>
                        <div class="controls">
                            <textarea class="" name="product_generic_name" id="textarea2" required rows="3" style="width:500px; height:40px;"><?php //echo $message_info->message_list;?></textarea>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <button type="reset" class="btn">Cancel</button>
                    </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->
