<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i>Visualization Content Edit</h2>
            <div class="box-icon">
                <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>
        
        <div class="box-content">
            <div style="color:green; font-size: 16px;">
            <?php 
             $msg=$this->session->userdata('message');
             if($msg)
             {
               echo $msg;
               $this->session->unset_userdata('message');
             }
            ?>
            
            </div>
            <form class="form-horizontal" action="<?php echo base_url(); ?>administrator/update_visualization" method="post" enctype="multipart/form-data">
                <fieldset>
                    <legend>
                        
                    </legend>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Image Title(<span class="required">*</span>)</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="typeahead" required data-provide="typeahead" data-items="4" name="title" value="<?php echo $visualization_info->title;?>">
                            <input type="hidden" class="span6 typeahead" id="typeahead" required data-provide="typeahead" data-items="4" name="image_id" value="<?php echo $visualization_info->image_id;?>">

                            <p class="help-block"></p>
                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label" for="fileInput">Previous Image</label>
                        <div class="controls">
                            <img src="<?php echo base_url().$visualization_info->image;?>" width="400" height="200"> 
                        </div>
                    </div> 
                    
                    
                    <div class="control-group">
                        <label class="control-label" for="fileInput">Image</label>
                        <div class="controls">
                            <input type="file" name="image" accept="image/*"><span style="color: green;">(N.B:File size should not be more than 3 MB and 2000*1000 pixel)</span>
                        </div>
                    </div>          
                    
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <button type="reset" class="btn">Cancel</button>
                    </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->


