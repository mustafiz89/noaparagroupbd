<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-user"></i>Manage Pharmacy Product</h2>
            <div class="box-icon">
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>
        
        <div class="box-content">
            <div style="color:green; font-size: 20px">
            <?php
             $msg=$this->session->userdata('message');
             if($msg)
             {
                 echo $msg;
                 $this->session->unset_userdata('message');
             }
            ?>
            </div>
            <br>
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Product Name</th>
                        <th>Product Generic Name</th>
                        <th>Actions</th>
                    </tr>
                </thead>   
                <tbody>

                    <?php
                    
                    $i=0;
                    foreach ($all_product as $v_product) {
                       $i+=1;
                        ?>
                        <tr>
                            <td class="center"><?php echo $i;?></td>
                            <td class="center"><?php echo $v_product->product_name; ?></td>
                            <td class="center"><?php echo $v_product->product_generic_name; ?></td>
                            
                            <td class="center">
                                <a class="btn btn-info" href="<?php echo base_url();?>administrator/edit_Pharmacy_product/<?php echo $v_product->product_id;?>">
                                    <i class="icon-edit icon-white"></i>  
                                    Edit                                            
                                </a> 
                                
                                <a class="btn btn-danger" href="<?php echo base_url(); ?>administrator/delete_pharmacy_product/<?php echo $v_product->product_id; ?>" onclick="return check_delete();">
                                    <i class="icon-trash icon-white"></i> 
                                    Delete
                                </a>
                            </td>
                                                   
                            
                        </tr>
                    <?php
                        }
                    ?>

                </tbody>
            </table>
        </div>
    </div>
</div>
