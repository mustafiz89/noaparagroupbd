<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i>Pharmacy Product Edit</h2>
            <div class="box-icon">
                <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>
        
        <div class="box-content">
            
            
            <form class="form-horizontal" action="<?php echo base_url(); ?>administrator/update_pharmacy_product" method="post" enctype="multipart/form-data">
                <fieldset>
                    <legend>
                        
                    </legend>
                    <div class="control-group">
                        <label class="control-label" for="textarea2">Product Name(<span class="required">*</span>)</label>
                        <div class="controls">
                            <textarea class="" name="product_name" id="textarea2" required rows="3" style="width:500px; height:40px;" maxlength="250"><?php echo $product_info->product_name;?></textarea>
                            <input type="hidden" class="span6 typeahead" id="typeahead" required data-provide="typeahead" data-items="4" name="product_id" value="<?php echo $product_info->product_id;?>">

                        </div>
                    </div>
                    
                    <div class="control-group">
                        <label class="control-label" for="textarea2">Product Generic Name(<span class="required">*</span>)</label>
                        <div class="controls">
                            <textarea class="" name="product_generic_name" id="textarea2" required rows="3" style="width:500px; height:40px;"><?php echo $product_info->product_generic_name;?></textarea>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <button type="reset" class="btn">Cancel</button>
                    </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->
