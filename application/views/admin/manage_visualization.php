<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-user"></i>Manage Visualization</h2>
            <div class="box-icon">
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>
        
        <div class="box-content">
            <div style="color:green; font-size: 20px">
            <?php
             $msg=$this->session->userdata('message');
             if($msg)
             {
                 echo $msg;
                 $this->session->unset_userdata('message');
             }
            ?>
            </div>
            <br>
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Image</th>
                        <th>Actions</th>
                    </tr>
                </thead>   
                <tbody>

                    <?php
                    
                    $i=0;
                    foreach ($all_visualization as $v_visualization) {
                       $i+=1;
                        ?>
                        <tr>
                            <td class="center"><?php echo $i;?></td>
                            <td class="center"><?php echo $v_visualization->title; ?></td>
                            <td class="center">
                                <img src="<?php echo base_url().$v_visualization->image;?>" width="200" height="100" >
                            </td>
                            <td class="center">
                                <a class="btn btn-info" href="<?php echo base_url();?>administrator/edit_visualization/<?php echo $v_visualization->image_id;?>">
                                    <i class="icon-edit icon-white"></i>  
                                    Edit                                            
                                </a> 
                                
                                <a class="btn btn-danger" href="<?php echo base_url(); ?>administrator/delete_visualization/<?php echo $v_visualization->image_id; ?>" onclick="return check_delete();">
                                    <i class="icon-trash icon-white"></i> 
                                    Delete
                                </a>
                            </td>
                                                   
                            
                        </tr>
                    <?php
                        }
                    ?>

                </tbody>
            </table>
        </div>
    </div>
</div>
