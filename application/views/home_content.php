<!--Slider Part-->

<div class="slider_wrapper">
    <div id="camera_wrap" class="">
        <?php
                foreach($all_slider as $v_slider)
                {
                ?>
        <div data-src="<?php echo base_url().$v_slider->slider_image; ?>">
        </div>
        <?php
                }
                ?>
    </div>
</div>



<div class="content">
  <div class="container_12">
<!--Welcome Note-->
    <div class="grid_12">
      <h2><?php echo $welcome_message->welcome_short_message;?>
      <span><?php echo $welcome_message->welcome_long_message;?> </span>
      </h2>
    </div>
<!--Welcome Note-->
    <div class="clear"></div>

	
	
	<!--Message-->
    <div class="grid_12">
      <h3><span>Management's Message</span></h3>
    </div>
      <div class="grid_6">
        <blockquote>
          <img src="<?php echo base_url().$chairman_message->image;?>" alt="" class="img_inner fleft">
          <div class="extra_wrapper">
          <span class="col2 upp"><?php echo $chairman_message->chairman_name;?> </span><br>- Chairman
            <p><?php echo $chairman_message->message_short_list;?></p>
            <span><a href="<?php echo base_url();?>welcome/chairman/<?php echo $chairman_message->message_id;?>" class="details">Read More</a></span>
          </div>
        </blockquote>
      </div>
      <div class="grid_6">
        <blockquote>
          <img src="<?php echo base_url().$md_message->image;?>" alt="" class="img_inner fleft">
          <div class="extra_wrapper">
            <span class="col2 upp"><?php echo $md_message->md_name;?></span> <br>- Managing Director
            <p><?php echo $md_message->message_short_list;?></p>
          <span><a href="<?php echo base_url();?>welcome/md/<?php echo $md_message->message_id;?>" class="details">Read More</a></span>
          </div>
        </blockquote>
      </div>

	  
	  
	  
	  <!--Message-->
    <div class="clear"></div>
    <div class="grid_12">
      <h3><span>Mission & Vision</span></h3>
    </div>
    <div class="clear"></div>

	
	
	<!--Mision & Vision-->
    <div id="mission">
        <div class="mission">
            <div class="heading" style="margin-top: 10px;">
                Vision
            </div>
            <div class="missiondetails">
                 Be the leader in marketing of all sorts of chemical fertilizer and manufacturing of mixed 	fertilizer & agro based products and serves our most valuable end users.
            </div> 
        </div>
        <div class="mission">
            <div class="heading">
                Mission
            </div>
            <div class="missiondetails">
                 Quality product through import and manufacturing with a view to maximize satisfaction of the ultimate user.
            </div> 
        </div>
        
        
    </div>
<!--Mision & Vision-->





<!--Sister Concern-->   
    <div class="grid_12">
      <h3><span>Sister Concern</span></h3>
    </div> 
    <div class="grid_4">
      <div class="icon">
          <div class="port">
            <a href="images/concern/1.jpg" class="gal"><img src="images/concern/1.jpg" alt="Sister Concern"></a>
          </div>
        <div class="title"><a href="#">SOUTH BENGAL FERTILIZER MILLS. LTD.</a>
        </div>
      <p>South Bengal Fertilizer mills Ltd, a sister concern of the group is established as a private Company limited by shares, located at Mohakal, Noapara, Avoynagar, Jessore, Bangladesh.</p>
        
      </div>
    </div>
    <div class="grid_4">
      <div class="icon">
        <div class="port">
            <a href="images/concern/2.jpg" class="gal"><img src="images/concern/2.jpg" alt="Sister Concern"></a>
        </div>
        <div class="title"><a href="#">NOAPARA CEMENT MILLS LTD.</a>
      </div>
      <p>Noapara Cement Mills Ltd. is a Cement Grinding Plant located on the river Bhairab at Noapara Area. OPC and Composite Cement is now producing here by applying latest computer technology.</p>
        
      </div>
    </div>
    <div class="grid_4">
      <div class="icon">
          <div class="port">
            <a href="images/concern/3.jpg" class="gal"><img src="images/concern/3.jpg" alt="Sister Concern"></a>
          </div>
        <div class="title"><a href="#">CONCORD PHARMACEUTICALS LTD.</a>
      </div>
      <p>CONCORD Pharmaceuticals Ltd. the fast growing company is committed to manufacturing of quality medicines through strict adherence to internationally accepted standards of Good Manufacturing Practice.</p>
        
      </div>
    </div>
<!--Sister Concern--> 
  </div>
</div> 




