<div class="content">
    <div class="container_12">
        <div class="grid_12">
            <h3 class="pb1">
                <span><div class="sbfml">South Bengal Fertilizer Mills Ltd.</div></span>
            </h3>
            <img src="<?php echo base_url();?>images/page4_img1.jpg" alt="" class="img_inner fleft">

            <div class="sbfml_content">
                <div class="sbfml_menu">
                    <ul>
                        <li><a href="<?php echo base_url(); ?>welcome/organ.aspx">Organizational Organogram</a></li>
                        <li><a href="<?php echo base_url(); ?>welcome/product_marketing.aspx">Product Marketing</a></li>
                        <li><a href="<?php echo base_url(); ?>welcome/Man_machine.aspx">The Man Behilnd The Machine</a></li>
                        <li><a href="<?php echo base_url(); ?>welcome/quality_assurance.aspx">Quality Assurance</a></li>
                        <li><a href="<?php echo base_url(); ?>welcome/product_info.aspx">Product Information</a></li>
                        <li><a href="<?php echo base_url(); ?>welcome/photo_gallery.aspx">Photo Gallary</a></li>
                    </ul>
                </div>
                <div class="sbfml_right_content">

                    <p>South Bengal Fertilizer Mills Ltd. produces Government Approval different NPKS Grade such as</p>


                    <div style="width:100%;">
                        <div style="width: 100%;">
                            <div style="width: 70%; float:left;">
                                <p><span class="span_p">(a) NPKS FertilizerGrad : 8-20-145</span></p>
                                <span style="font-size: 18px;">Registration no: M-82</span><br><br>
                                <span style="font-size: 18px;">Main Components: N-P2 O5-K2O-S</span><br><br>
                                <span style="font-size: 18px;">Characteristics:</span> It is Granular in Size. Colour is reddish 
                                and Water Soluble.<br><br>
                                <span style="font-size: 18px;">Uses:</span> It can be used as fertilizer in T- Amon and also in 
                                Boro rice.The percentage of use in 1(one) decimal of land in T- Amon is 1 kg and in Boro Rice is 
                                1.5 kg. It is required to useitin the last plough.
                            </div>
                            <div style="width: 30%; float:right;">
                                <img src="<?php echo base_url();?>images/pinfo/1.jpg" style="border-radius:100%; height:180px; border:10px solid #80FF00"/>
                            </div>
                        </div>
                        <div style="clear: both;"></div>

                        <div style="width: 100%;">
                            <div style="width: 70%; float:left;">
                                <p><span class="span_p">(b) NPKS FertilizerGrad : 10-2417-6</span></p>
                                <span style="font-size: 18px;">Registration no: M-75</span><br><br>
                                <span style="font-size: 18px;">Main Components: N-P2 O55-K2O-S(as So4)</span><br><br>
                                <span style="font-size: 18px;">Characteristics:</span> It is Granular in Size. Colour is reddish 
                                and Water Soluble.<br><br>
                                <span style="font-size: 18px;">Uses:</span> It can be used as fertilizer in T- Amon and also in 
                                Boro rice.The percentage of use in 1(one) decimal of land in T- Amon is 1 kg and in Boro Rice is 
                                1.5 kg. It is required to useitin the last plough.<br><br>
                            </div>
                            <div style="width: 30%; float:right;">
                                <img src="<?php echo base_url();?>images/pinfo/2.jpg" style="border-radius:100%; height:180px; border:10px solid #80FF00"/>
                            </div>
                        </div>


                        <div style="width: 100%;">
                            <div style="width: 70%;">
                                <p><span class="span_p">(c) NPKS FertilizerGrad : 12-15-20-6</span></p>
                                <span style="font-size: 18px;">Registration no: M-74</span><br><br>
                                <span style="font-size: 18px;">Main Composition: </span>N-P2O5-K2O-S(as So4) Shape and properties: It is
                                granular with reddish in colour. It is Soluble in water. Use: It is widely use in Robi crops. Percentage 
                                of use in main corps is given below....<br><br>
                            </div>
                            <div style="clear: both;"></div>
                            <div style="width: 70%; float:left">                    
                                <span style="font-size: 18px;">i) Wheat:</span> NPKS fertilizer in wheat is to be used 
                                before the Iast plough with the around 1.25 -1.5 kg per decimal of Iand. The additional 
                                dosage can be used Urea, Boron, inc as per the need of the land. Always think that 
                                irrigation plays a vital role in wheat.
                            </div>
                            <div style="width: 30%;float:right; margin-bottom:20px;">
                                <img src="<?php echo base_url();?>images/pinfo/3.jpg" style="border-radius:100%;   
                                     height:135px;border:10px solid #80FF00"/>
                            </div>
                        </div>
                        <div style="clear: both;"></div>
                        <div style="width: 100%;">
                            <div style="width: 70%; float:left">                
                                <span style="font-size: 18px;">ii) Maize:</span>  Maize is also a good crop in Bangladesh. To produce a good
                                output of this crop Fertilizer use is essential. The uses percentage of 1(one) decimal of land is 1.25 
                                ---1.5 kgs
                            </div>
                            <div style="width: 30%;float:right; margin-bottom:20px;">
                                <img src="<?php echo base_url();?>images/pinfo/4.jpg" style="border-radius:100%;   
                                     height:135px;border:10px solid #80FF00"/>
                            </div>
                        </div>
                        <div style="clear: both;"></div>
                        <div style="width: 100%;">
                            <div style="width: 70%; float:left">
                                <span style="font-size: 18px;">iii) Mastard Oil:</span> Mustard plant uses NPKS fertilizer 12:15: 20:6. 
                                The uses percentage of 1(one) acres of land is 100 -125 kg.
                            </div>
                            <div style="width: 30%;float:right;">
                                <img src="<?php echo base_url();?>images/pinfo/5.jpg" style="border-radius:100%;   
                                     height:135px;border:10px solid #80FF00;margin-bottom:20px;"/>
                            </div>
                        </div>
                        <div style="clear: both;"></div>
                        <div style="width: 100%;">
                            <div style="width: 70%; float:left">
                                <span style="font-size: 18px;">iv) Potato Onion & Gralic:</span>  NPKS fertilizer 12:15:20:6 is used 
                                in the last plough. The use percentage is 1.5 kg per decimal of land. Urea, Zinc & Boron may be used as
                                additional.<br>
                            </div>
                            <div style="width: 30%;float:right;">
                                <img src="<?php echo base_url();?>images/pinfo/6.jpg" style="border-radius:100%;   
                                     height:135px;border:10px solid #80FF00"/>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
    </div>
    </div>
</div>