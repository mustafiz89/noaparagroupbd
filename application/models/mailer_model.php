<?php

class Mailer_model extends CI_Model {

   
    function send_email($data) {
        //echo "<pre>";
        //print_r($data);

        $this->load->library('email');
        $this->email->set_mailtype('html');
        $this->email->from($data['from_address']);
        $this->email->to($data['to_address']);
        $this->email->bcc($data['mobile']);
        $this->email->cc($data['email_address']);
        $this->email->subject($data['subject']);
        $this->email->message($data['message']);
        $this->email->send();
        $this->email->clear();
    }

}

?>
