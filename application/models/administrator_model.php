<?php

Class Administrator_model extends CI_Model{
    
    public function welcome_message_info()
    {
        $this->db->select('*');
        $this->db->from('tbl_welcome_message');
        //$this->db->where('welcome_id',$welcome_id);
        $query_result=$this->db->get();
        $result=$query_result->result();
        return $result;  
    }
    
    public function select_welcome_message_by_id($welcome_id)
    {
         $this->db->select('*');
        $this->db->from('tbl_welcome_message');
        $this->db->where('welcome_id',$welcome_id);
        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result;  
                
    }
    
    public function update_welcome_message_by_id($data,$welcome_id)
    {
        $this->db->where('welcome_id',$welcome_id);
        $this->db->update('tbl_welcome_message',$data);
        
        
    }
    
    
    /* slider */
    
    public function save_slider_info($data)
    {
        $this->db->insert('tbl_slider',$data);        
    }
    
    public function select_all_slider()
    {
      $this->db->select('*');
      $this->db->from('tbl_slider');
      $query_result=$this->db->get();
      $result=$query_result->result();
      return $result;
    }
    
    public function select_slider_by_id($slider_id)
  {
      $this->db->select('*');
      $this->db->from('tbl_slider');
      $this->db->where('slider_id',$slider_id);
      $query_result=$this->db->get();
      $result=$query_result->row();
      return $result;
  }
    
    public function delete_slider_by_id($slider_id) 
    {
        $this->db->where('slider_id', $slider_id);
        $this->db->delete('tbl_slider');
    }
    
    
    /* chairman message */
    
    public function chairman_message_info()
    {
      $this->db->select('*');
      $this->db->from('tbl_chairman_message');
      $query_result=$this->db->get();
      $result=$query_result->result();
      return $result;
    }
    public function select_chairman_message_by_id($message_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_chairman_message');
        $this->db->where('message_id',$message_id);
        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result;  
    }
    
    public function update_chairman_message($data,$message_id)
    {
        $this->db->where('message_id',$message_id);
        $this->db->update('tbl_chairman_message',$data);
    }
    
    /* director message */
    
    public function md_message_info()
    {
      $this->db->select('*');
      $this->db->from('tbl_md_message');
      $query_result=$this->db->get();
      $result=$query_result->result();
      return $result;
    }
    public function select_md_message_by_id($message_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_md_message');
        $this->db->where('message_id',$message_id);
        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result;  
    }
    
    public function update_md_message($data,$message_id)
    {
        $this->db->where('message_id',$message_id);
        $this->db->update('tbl_md_message',$data);
    }
    
    /* Pharmacy Product list */
    
    public function save_pharmacy_product($data)
    {
        $this->db->insert('tbl_pharmacy_product',$data);
        
    }
    
    public function select_all_product()
    {
        $this->db->select('*');
        $this->db->from('tbl_pharmacy_product');
        //$this->db->where('welcome_id',$welcome_id);
        $this->db->order_by("product_name","asec");
        $query_result=$this->db->get();
        
        $result=$query_result->result();
        return $result;  
    }
    public function select_product_by_id($product_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_pharmacy_product');
        $this->db->where('product_id',$product_id);
        $query_result=$this->db->get();        
        $result=$query_result->row();
        return $result; 
    }
    
    public function update_product_by_id($product_id,$data)
    {
        $this->db->where('product_id',$product_id);
        $this->db->update('tbl_pharmacy_product',$data);
    }
    
    public function delete_pharmacy_product_by_id($product_id)
    {
        $this->db->where('product_id',$product_id);
        $this->db->delete('tbl_pharmacy_product');        
    }
    
    /*start visualization */
    
    public function save_visualization_info($data)
    {
       $this->db->insert('tbl_visualization',$data); 
    }
    
    public function select_all_visualization()
    {
        $this->db->select('*');
        $this->db->from('tbl_visualization');
        $query_result=$this->db->get();
        $result=$query_result->result();
        return $result;  
    }
    
    public function select_visualization_by_id($image_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_visualization');
        $this->db->where('image_id',$image_id);
        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result; 
    }
    
    public function update_visualization($image_id,$data)
    {
        $this->db->where('image_id',$image_id);
        $this->db->update('tbl_visualization',$data);
    }
    
    public function delete_visualization_by_id($image_id)
    {
        $this->db->where('image_id',$image_id);
        $this->db->delete('tbl_visualization');        
    }
    
    /*End visualization */
    
    /*start photo galllary */
    
    public function save_photo_gallary_info($data)
    {
        $this->db->insert('tbl_photo_gallary',$data);
    }
    
    public function select_all_photo()
    {
        $this->db->select('*');
        $this->db->from('tbl_photo_gallary');
        $query_result=$this->db->get();
        $result=$query_result->result();
        return $result;  
    }
    public function select_photo_gallary_by_id($photo_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_photo_gallary');
        $this->db->where('photo_id',$photo_id);
        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result;
    }
    public function delete_photo_gallary_by_id($photo_id)
    {
        $this->db->where('photo_id',$photo_id);
        $this->db->delete('tbl_photo_gallary');
    }
}
