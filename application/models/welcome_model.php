<?php

Class Welcome_model extends CI_Model{
    
    public function get_all_slider()
    {
        $this->db->select('*');
        $this->db->from('tbl_slider');
       // $this->db->where('welcome_id',$welcome_id);
        $query_result=$this->db->get();
        $result=$query_result->result();
        return $result;  
        
    }
    public function welcome_message_info($welcome_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_welcome_message');
        $this->db->where('welcome_id',$welcome_id);
        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result;  
    }
    
    public function chairman_message_info($message_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_chairman_message');
        $this->db->where('message_id',$message_id);
        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result;  
        
    }
    
    public function md_message_info($message_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_md_message');
        $this->db->where('message_id',$message_id);
        $query_result=$this->db->get();
        $result=$query_result->row();
        return $result;  
        
    }
    
    public function select_all_visualization_info()
    {
        $this->db->select('*');
        $this->db->from('tbl_visualization');
        $query_result=$this->db->get();
        $result=$query_result->result();
        return $result; 
        
    }
    
    public function select_all_photo()
    {
        $this->db->select('*');
        $this->db->from('tbl_photo_gallary');
        $query_result=$this->db->get();
        $result=$query_result->result();
        return $result;         
    }
    public function select_all_product()
    {
        $this->db->select('*');
        $this->db->from('tbl_pharmacy_product');
        $this->db->order_by("product_name","asec");
        $query_result=$this->db->get();
        
        $result=$query_result->result();
        return $result; 
    }
}