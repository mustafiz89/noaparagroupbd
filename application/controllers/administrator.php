<?php 
session_start();
if(!defined('BASEPATH')) exit ('No direct script access allowed');

Class Administrator extends CI_Controller{
    public function __construct() {
        parent::__construct();
        $admin_id=$this->session->userdata('admin_id');
        if($admin_id==null)
        {
            redirect('noapara_login','refresh');
        }
        
    }
    
    public function index()
    {
        $data=array();
        $data['title']='Dashboard';
        $data['admin_mid_content']=$this->load->view('admin/dashboard',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    
    public function logout()
    {
        $this->session->unset_userdata('admin_name');
        $this->session->unset_userdata('admin_id');
        $sdata=array();
        $sdata['message']='You are successfully logout';
        $this->session->set_userdata($sdata);
        redirect('noapara_login','refresh');
    }
    
    
    
    /* Slider start */
    
    public function add_slider()
    {
        $data=array();
        $data['title']='Add Slider';
        $data['admin_mid_content']=$this->load->view('admin/add_slider',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    
    public function save_slider() {
        
        $data = array();
            /* upload image */

            $config['upload_path'] = 'images/slider/';
            $config['allowed_types'] = 'gif|jpg|jpeg|png';
            $config['max_size'] = '3000';
            $config['max_width'] = '2000';
            $config['max_height'] = '1000';
            $error = '';
            $fdata = array();

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('slider_image')) {
                $error = $this->upload->display_errors();
            } else {

                $fdata = $this->upload->data();
                $data['slider_image'] = $config['upload_path'] . $fdata['file_name'];
            }

//        echo'<pre>';
//        print_r($data);
//        exit();

            $this->administrator_model->save_slider_info($data);
            $sdata = array();
            $sdata['message'] = 'Save Slider Successfully';
            $this->session->set_userdata($sdata);
            redirect('administrator/add_slider');
    } 
        
    
    
    public function manage_slider()
    {
        $data=array();
        $data['title']='Manage Slider';
        $data['all_slider']=$this->administrator_model->select_all_slider();
        $data['admin_mid_content']=$this->load->view('admin/manage_slider',$data,true);
        $this->load->view('admin/admin_master',$data);
        
    }
    
    public function delete_slider($slider_id)
    {
        $db_slider_image=$this->administrator_model->select_slider_by_id($slider_id);
         
         if($db_slider_image->slider_image)
         {
             $image=$db_slider_image->slider_image;
             unlink($image);        
         }
                 
         $this->administrator_model->Delete_slider_by_id($slider_id);
          $sdata=array();
            $sdata['message']='Delete Information Successfully';
            $this->session->set_userdata($sdata);
         redirect('administrator/manage_slider');   
        
    }
    
    /* slider End */
    
    /*welcome message start*/
    
    
    public function welcome_message()
    {
        $data=array();
        $data['title']='Welcome Massage';
        $data['welcome_info']=$this->administrator_model->welcome_message_info();
//        echo'<pre>';
//        print_r($data['welcome_info']);
//        exit();
        $data['admin_mid_content']=$this->load->view('admin/welcome_message',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    
    public function welcome_message_edit($welcome_id)
    {
        $data=array();
        $data['title']='Customize Welcome Message';
        $data['edit_welcome_info']=$this->administrator_model->select_welcome_message_by_id($welcome_id);
//        echo'<pre>';
//        print_r($data['edit_welcome_info']);
//        exit();
         $data['admin_mid_content']=$this->load->view('admin/edit_welcome_message',$data,true);
        $this->load->view('admin/admin_master',$data);
        
    }
    
    public function update_welcome_message()
    {
        $data=array();
        $welcome_id=$this->input->post('welcome_id',true);
        $data['welcome_short_message']=$this->input->post('welcome_short_message',true);
        $data['welcome_long_message']=$this->input->post('welcome_long_message',true);
        $this->administrator_model->update_welcome_message_by_id($data,$welcome_id);
        $sdata=array();
        $sdata['message']='Update Information Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator/welcome_message');
    }
    
    
    /*welcome message end*/
    
    /*Chariman message */
    
    public function chairman_message()
    {
        $data=array();
        $data['title']='Chairman Message';
        $data['message_info']=$this->administrator_model->chairman_message_info();
        $data['admin_mid_content']=$this->load->view('admin/chairman_message',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    
    public function chairman_message_edit($message_id)
    {
        $data=array();
        $data['title']='Customize Chairman Message';
        $data['message_info']=$this->administrator_model->select_chairman_message_by_id($message_id);
//        echo'<pre>';
//        print_r($data['edit_welcome_info']);
//        exit();
         $data['admin_mid_content']=$this->load->view('admin/edit_chairman_message',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    
    public function update_chairman_message()
    {
        $data=array();
         $message_id=$this->input->post('message_id',true);
         $data['chairman_name']=$this->input->post('chairman_name',true); 
         $data['message_short_list']=$this->input->post('message_short_list',true);
         $data['message_list']=$this->input->post('message_list',true);
        
         $db_image=$this->administrator_model->select_chairman_message_by_id($message_id);
          
         
         $config['upload_path']='images/';
         $config['allowed_types']='jpeg|jpg|gif|png';
         $config['max_size']='3000';
         $config['max_width']='2000';
         $config['max_height']='1000';
         //$config['remove_spaces']=true;
         $error='';
         $fdata=array();
         
         $this->load->library('upload',$config);
         
         if(!$this->upload->do_upload('image'))
         {
             $error=$this->upload->display_errors();
             //echo $error;
         }
         else {
        
            $fdata = $this->upload->data();
        
                if ($fdata['file_name'] == '') 
                {
                    return;
                } 
                else {
                    
                   if($db_image->image)
                        {
                            $image=$db_image->image;
                            unlink($image);        
                        }

                    
                    $data['image'] = $config['upload_path'] . $fdata['file_name'];

                }
         
                        
        }
        
          
        
         $this->administrator_model->update_chairman_message($data,$message_id);
         $sdata=array();
         $sdata['message']='Update service Successfully';
         $this->session->set_userdata($sdata);
         redirect('administrator/chairman_message');
            
        
    }
    
    /* Director's Message */
    
    
    
    public function md_message()
    {
        $data=array();
        $data['title']='M.D Message';
        $data['message_info']=$this->administrator_model->md_message_info();
        $data['admin_mid_content']=$this->load->view('admin/md_message',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    
    public function md_message_edit($message_id)
    {
        $data=array();
        $data['title']='Customize Chairman Message';
        $data['message_info']=$this->administrator_model->select_md_message_by_id($message_id);
//        echo'<pre>';
//        print_r($data['edit_welcome_info']);
//        exit();
         $data['admin_mid_content']=$this->load->view('admin/edit_md_message',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    
    public function update_md_message()
    {
        $data=array();
         $message_id=$this->input->post('message_id',true);
         $data['md_name']=$this->input->post('md_name',true); 
         $data['message_short_list']=$this->input->post('message_short_list',true);
         $data['message_list']=$this->input->post('message_list',true);
        
         $db_image=$this->administrator_model->select_md_message_by_id($message_id);
          
         
         $config['upload_path']='images/';
         $config['allowed_types']='jpeg|jpg|gif|png';
         $config['max_size']='3000';
         $config['max_width']='2000';
         $config['max_height']='1000';
         //$config['remove_spaces']=true;
         $error='';
         $fdata=array();
         
         $this->load->library('upload',$config);
         
         if(!$this->upload->do_upload('image'))
         {
             $error=$this->upload->display_errors();
             //echo $error;
         }
         else {
        
            $fdata = $this->upload->data();
        
                if ($fdata['file_name'] == '') 
                {
                    return;
                } 
                else {
                    
                   if($db_image->image)
                        {
                            $image=$db_image->image;
                            unlink($image);        
                        }

                    
                    $data['image'] = $config['upload_path'] . $fdata['file_name'];

                }
         
                        
        }
        
          
        
         $this->administrator_model->update_md_message($data,$message_id);
         $sdata=array();
         $sdata['message']='Update service Successfully';
         $this->session->set_userdata($sdata);
         redirect('administrator/md_message');
            
        
    }
   
    
    /*Pharmacy Product list */
    
    public function add_pharmacy_product()
    {
        $data=array();
        $data['title']='Add Product';
        $data['admin_mid_content']=$this->load->view('admin/add_pharmacy_product',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    
    public function save_pharmacy_product()
    {
        $data=array();
        $data['product_name']=$this->input->post('product_name',true);
        $data['product_generic_name']=$this->input->post('product_generic_name',true);
        $this->administrator_model->save_pharmacy_product($data);
        $sdata=array();
        $sdata['message']='Save Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator/add_pharmacy_product');
    }
    
    public function manage_pharmacy_product()
    {
        $data=array();
        $data['title']='Manage Product';
        $data['all_product']=$this->administrator_model->select_all_product();
        $data['admin_mid_content']=$this->load->view('admin/manage_pharmacy_product',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    
    public function edit_pharmacy_product($product_id)
    {
        $data=array();
        $data['title']='Edit Product Info';
        $data['product_info']=$this->administrator_model->select_product_by_id($product_id);
        $data['admin_mid_content']=$this->load->view('admin/edit_pharmacy_product',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    
    public function update_pharmacy_product()
    {
        $data=array();
        $product_id=$this->input->post('product_id',true);
        $data['product_name']=$this->input->post('product_name',true);
        $data['product_generic_name']=$this->input->post('product_generic_name',true);
        $this->administrator_model->update_product_by_id($product_id,$data);
        $sdata=array();
        $sdata['message']='Update Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator/manage_pharmacy_product');
    }
    
    public function delete_pharmacy_product($product_id)
    {
        $data=array();
        $this->administrator_model->delete_pharmacy_product_by_id($product_id);
        $sdata=array();
        $sdata['message']='Delete Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator/manage_pharmacy_product');
    }
    /* start visualizaiton */
    
    public function add_visualization()
    {
        $data=array();
        $data['title']='visualization';
        $data['admin_mid_content']=$this->load->view('admin/add_visualization',$data,true);
        $this->load->view('admin/admin_master',$data);
        
    }
    
    public function save_visualization()
    {
        $data = array();
        $data['title']=$this->input->post('title',true);
            /* upload image */

            $config['upload_path'] = 'images/gallery/';
            $config['allowed_types'] = 'gif|jpg|jpeg|png';
            $config['max_size'] = '3000';
            $config['max_width'] = '2000';
            $config['max_height'] = '1000';
            $error = '';
            $fdata = array();

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('image')) {
                $error = $this->upload->display_errors();
            } else {

                $fdata = $this->upload->data();
                $data['image'] = $config['upload_path'] . $fdata['file_name'];
            }

//        echo'<pre>';
//        print_r($data);
//        exit();

            $this->administrator_model->save_visualization_info($data);
            $sdata = array();
            $sdata['message'] = 'Save Successfully';
            $this->session->set_userdata($sdata);
            redirect('administrator/add_visualization');
    }
    
    public function manage_visualization()
    {
        $data=array();
        $data['title']='Manage visualization Image';
        $data['all_visualization']=$this->administrator_model->select_all_visualization();
//        echo'<pre>';
//        print_r($data['all_visualization']);
//        exit();
        $data['admin_mid_content']=$this->load->view('admin/manage_visualization',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    
    public function edit_visualization($image_id)
    {
        $data=array();
        $data['title']='Edit visualization Info';
        $data['visualization_info']=$this->administrator_model->select_visualization_by_id($image_id);
        $data['admin_mid_content']=$this->load->view('admin/edit_visualization',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    
    public function update_visualization()
    {
        $data=array();
        $image_id=$this->input->post('image_id',true);
        $data['title']=$this->input->post('title',true);
        $db_image=$this->administrator_model->select_visualization_by_id($image_id);
        
        /*upload image*/
        
        $config['upload_path'] = 'images/gallery/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['max_size'] = '3000';
        $config['max_width'] = '2000';
        $config['max_height'] = '1000';
        $error = '';
        $fdata = array();
        
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('image')) {
            $error = $this->upload->display_errors();
            //echo $error;
        } else {

            $fdata = $this->upload->data();

            if ($fdata['file_name'] == '') {
                return;
            } else {

                if ($db_image->image) {
                    $image = $db_image->image;
                    unlink($image);
                }


                $data['image'] = $config['upload_path'] . $fdata['file_name'];
            }
        }
    
         $this->administrator_model->update_visualization($image_id,$data);
         $sdata=array();
         $sdata['message']='Update Successfully';
         $this->session->set_userdata($sdata);
         redirect('administrator/manage_visualization');
        
    }
    
    
    public function delete_visualization($image_id)
    {
        $db_image=$this->administrator_model->select_visualization_by_id($image_id);
         
         if($db_image->image)
         {
             $image=$db_image->image;
             unlink($image);        
         }
        $data=array();
        $this->administrator_model->delete_visualization_by_id($image_id);
        $sdata=array();
        $sdata['message']='Delete Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator/manage_visualization');        
    }
    
    /* End Visualization */
    
    /*start Photo Gallary*/
    
    public function add_photo_gallary()
    {
        $data=array();
        $data['title']='Add Photo';
        $data['admin_mid_content']=$this->load->view('admin/add_photo_gallary',$data,true);
        $this->load->view('admin/admin_master',$data);
    }
    
    public function save_photo_gallary()
    {
            $data=array();
            $config['upload_path'] = 'images/sb_photo_gallary/';
            $config['allowed_types'] = 'gif|jpg|jpeg|png';
            $config['max_size'] = '3000';
            $config['max_width'] = '2000';
            $config['max_height'] = '1000';
            $error = '';
            $fdata = array();

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('image')) {
                $error = $this->upload->display_errors();
            } else {

                $fdata = $this->upload->data();
                $data['image'] = $config['upload_path'] . $fdata['file_name'];
            }

//        echo'<pre>';
//        print_r($data);
//        exit();

            $this->administrator_model->save_photo_gallary_info($data);
            $sdata = array();
            $sdata['message'] = 'Save Successfully';
            $this->session->set_userdata($sdata);
            redirect('administrator/add_photo_gallary');
        
    }
    
    public function manage_photo_gallary()
    {
        $data=array();
        $data['title']='Manage Photo';
        $data['all_photo']=$this->administrator_model->select_all_photo();
        $data['admin_mid_content']=$this->load->view('admin/manage_photo_gallary',$data,true);
        $this->load->view('admin/admin_master',$data);
        
    }
    
    public function delete_photo_gallary($photo_id)
    {
        $db_image=$this->administrator_model->select_photo_gallary_by_id($photo_id);
         
         if($db_image->image)
         {
             $image=$db_image->image;
             unlink($image);        
         }
        
        $data=array();
        $this->administrator_model->delete_photo_gallary_by_id($photo_id);
        $sdata=array();
        $sdata['message']='Delete Successfully';
        $this->session->set_userdata($sdata);
        redirect('administrator/manage_photo_gallary');
        
        
    }
}