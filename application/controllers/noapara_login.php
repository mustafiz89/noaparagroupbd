<?php
session_start();
if (!defined('BASEPATH'))exit('No direct script access allowed');

Class Noapara_login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $admin_id = $this->session->userdata('admin_id');
        if ($admin_id != null) {
            redirect('administrator', 'refresh');
        }
    }

    public function index() {
        $this->load->view('admin/admin_login');
    }
    
    public function admin_login_check()
    {
        $admin_name=$this->input->post('admin_name',true);        
        $admin_password=$this->input->post('admin_password',true);        
        $result=$this->noapara_model->admin_login_check_info($admin_name,$admin_password);
                
//        echo '<pre>';
//           print_r($result);
//           exit();
//        send data to session
        
        $sdata=array();
        
        if($result)
        {
            $sdata['admin_name']=$result->admin_name;
            $sdata['admin_id']=$result->admin_id;
            $this->session->set_userdata($sdata);
            redirect('administrator','refresh');
        }
        else
        {
            $sdata['exception']='Invalid Username or Password !';
            $this->session->set_userdata($sdata);
            redirect('noapara_login');
        }
        
    }

}