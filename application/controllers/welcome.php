<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Welcome extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function index() {
        $data = array();
        $data['title'] = 'Noapara Group';
        $data['all_slider']=$this->welcome_model->get_all_slider();
        $data['welcome_message']=$this->welcome_model->welcome_message_info(1);
        $data['chairman_message']=$this->welcome_model->chairman_message_info(1);
        $data['md_message']=$this->welcome_model->md_message_info(1);
//        echo '<pre>';
//        print_r($data['welcome_message']);
//        exit();
        $data['mid_content'] = $this->load->view('home_content', $data, true);
        $this->load->view('master', $data);
    }
    public function chairman($message_id)
    {
        $data=array();
        $data['title']='Chariman Message';
         $data['chairman_message']=$this->welcome_model->chairman_message_info($message_id);
        $data['mid_content']=$this->load->view('chairman',$data,true);
        $this->load->view('master',$data);
    }
    public function md($message_id)
    {
        $data=array();
        $data['title']='Director Message';
        $data['md_message']=$this->welcome_model->md_message_info($message_id);
        $data['mid_content']=$this->load->view('md',$data,true);
        $this->load->view('master',$data);
    }

    public function profile() {
        $data = array();
        $data['title'] = 'Profile';
        $data['mid_content'] = $this->load->view('profile', '', true);
        $this->load->view('master', $data);
    }

    public function sbfml() {
        $data = array();
        $data['title'] = 'Sister Concern';
        $data['mid_content'] = $this->load->view('sbfml', '', true);
        $this->load->view('master', $data);
    }

    public function organ() {
        $data = array();
        $data['title'] = 'Noapara Group';
        $data['mid_content'] = $this->load->view('organ', '', true);
        $this->load->view('master', $data);
    }

    public function ncml() {
        $data = array();
        $data['title'] = 'Sister Concern';
        $data['mid_content'] = $this->load->view('ncml', $data, true);
        $this->load->view('master', $data);
    }

    public function cpl() {
        $data = array();
        $data['title'] = 'Sister Concern';
        $data['mid_content'] = $this->load->view('cpl', $data, true);
        $this->load->view('master', $data);
    }

    public function product_list() {
        $data = array();
        $data['title']='Product List';
        $data['all_product']=$this->welcome_model->select_all_product();
        $data['mid_content']=$this->load->view('product_list',$data,true);
        $this->load->view('master',$data);
        
    }
    
    public function product_marketing() {
        $data = array();
        $data['title']='Product Marketing';
        $data['mid_content']=$this->load->view('product_marketing',$data,true);
        $this->load->view('master',$data);
        
    }
    
     public function man_machine() {
        $data = array();
        $data['title']='Man Behind The Machine';
        $data['mid_content']=$this->load->view('man_machine',$data,true);
        $this->load->view('master',$data);
        
    }
     public function quality_assurance() {
        $data = array();
        $data['title']='Quality Assurance';
        $data['mid_content']=$this->load->view('quality_assurance',$data,true);
        $this->load->view('master',$data);
        
    }
    
     public function product_info() {
        $data = array();
        $data['title']='Product Information';
        $data['mid_content']=$this->load->view('product_info',$data,true);
        $this->load->view('master',$data);
        
    }
     public function photo_gallery() {
        $data = array();
        $data['title']='Photo Gallery';
        $data['all_photo']=$this->welcome_model->select_all_photo();
        $data['mid_content']=$this->load->view('photo_gallery',$data,true);
        $this->load->view('master',$data);
        
    }
    

    public function visualization() {
        $data = array();
        $data['title'] = 'Visualization';
        $data['v_info']=$this->welcome_model->select_all_visualization_info();
//        echo'<pre>';
//        print_r($data['v_info']);
//        exit();
        
        
        $data['mid_content'] = $this->load->view('visualization',$data,true);
        $this->load->view('master', $data);
    }

    public function contact_us() {
        $data = array();
        $data['title'] = 'Contact With Us';
        $data['mid_content'] = $this->load->view('contact_us', $data, true);
        $this->load->view('master', $data);
    }
    public function send_mail()
    {
        $mdata=array();
            $mdata['from_address']="mail@noaparagroupbd.com";            
            $mdata['to_address']="sohag2882@yahoo.com";
            $mdata['name']=$this->input->post('name');
            $mdata['mobile']=$this->input->post('mobile');
            $mdata['email_address']=$this->input->post('email_address');
            $mdata['subject']="Give information To U";
            $mdata['message']=$this->input->post('message');
            
//            echo '<pre>';
//            print_r($mdata);
//            exit();
            
            $this->mailer_model->send_email($mdata);
            $sdata=array();
            $sdata['message']='Successfully Send Message';
            $this->session->set_userdata($sdta);            
            redirect('welcome/contact_us');
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */