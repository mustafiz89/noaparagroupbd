-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 04, 2015 at 12:07 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dslbd_noapara`
--

-- --------------------------------------------------------

--
-- Table structure for table `menu_module`
--

CREATE TABLE IF NOT EXISTS `menu_module` (
`module_id` int(10) unsigned NOT NULL,
  `module_name` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu_module`
--

INSERT INTO `menu_module` (`module_id`, `module_name`) VALUES
(1, 'Home'),
(2, 'South Bengal'),
(3, 'Noapara Cement'),
(4, 'Concord Pharma'),
(5, 'Admin Setup'),
(6, 'myself');

-- --------------------------------------------------------

--
-- Table structure for table `noapara_bengal_photogallery`
--

CREATE TABLE IF NOT EXISTS `noapara_bengal_photogallery` (
`PICTURE_ID` int(100) NOT NULL,
  `PICTURE_TITLE` varchar(100) NOT NULL,
  `POST_BY` varchar(100) NOT NULL,
  `POST_DATE` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `noapara_bengal_photogallery`
--

INSERT INTO `noapara_bengal_photogallery` (`PICTURE_ID`, `PICTURE_TITLE`, `POST_BY`, `POST_DATE`) VALUES
(1, 'asf', 'demo', '2013-12-19'),
(2, 'raju', 'mahin', '2014-05-21');

-- --------------------------------------------------------

--
-- Table structure for table `noapara_bengal_product_marketing`
--

CREATE TABLE IF NOT EXISTS `noapara_bengal_product_marketing` (
`PRODUCT_ID` int(50) NOT NULL,
  `DESCRIPTION` longtext NOT NULL,
  `POST_BY` varchar(50) NOT NULL,
  `POST_DATE` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `noapara_bengal_product_marketing`
--

INSERT INTO `noapara_bengal_product_marketing` (`PRODUCT_ID`, `DESCRIPTION`, `POST_BY`, `POST_DATE`) VALUES
(1, 'Noapara Group produces Cement, Granular NPKS &amp; imports different \r\nitems of Fertilizer &amp; Food Grains like MOP,                DAP TSP, \r\nUrea, Rice, Sugar etc. and markets these products all over the country \r\nby their own marketing team.                The Organization also \r\nengages Dealers to sell these items. The following block diagram shows \r\nthe product                    marketing in a brief<br>', 'imran', '2013-12-19');

-- --------------------------------------------------------

--
-- Table structure for table `noapara_bengal_qa_photogallery`
--

CREATE TABLE IF NOT EXISTS `noapara_bengal_qa_photogallery` (
`QA_ID` int(100) NOT NULL,
  `PICTURE_TITLE` varchar(100) NOT NULL,
  `POST_BY` varchar(100) NOT NULL,
  `POST_DATE` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `noapara_bengal_qa_photogallery`
--

INSERT INTO `noapara_bengal_qa_photogallery` (`QA_ID`, `PICTURE_TITLE`, `POST_BY`, `POST_DATE`) VALUES
(2, 'This is a picture', 'imran', '2013-12-18'),
(3, 'fadsfa', 'imran', '2013-12-18'),
(4, 'dddd', 'imran', '2013-12-18'),
(5, 'The', 'imran', '2013-12-18');

-- --------------------------------------------------------

--
-- Table structure for table `noapara_bengal_the_man`
--

CREATE TABLE IF NOT EXISTS `noapara_bengal_the_man` (
`THE_MAN_ID` int(50) NOT NULL,
  `DESCRIPTION` longtext NOT NULL,
  `POST_BY` varchar(100) NOT NULL,
  `POST_DATE` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `noapara_bengal_the_man`
--

INSERT INTO `noapara_bengal_the_man` (`THE_MAN_ID`, `DESCRIPTION`, `POST_BY`, `POST_DATE`) VALUES
(1, 'The man behind the machines is Engineers, Trainde Technicians and \r\nSkilled workers. The technicians &amp; workers                 are \r\ntrainde in home in every six monthly. The quality product is introduced \r\nthrough trained of manpower and                 skilled them in every \r\nrespect. Owners are getting quality product through skilled manpower and\r\n Employees                    are trained up throuth skilled trainer.', 'imran', '2013-12-19');

-- --------------------------------------------------------

--
-- Table structure for table `noapara_bengal_visit_gallery`
--

CREATE TABLE IF NOT EXISTS `noapara_bengal_visit_gallery` (
`PICTURE_ID` int(50) NOT NULL,
  `PICTURE_TITLE` varchar(200) NOT NULL,
  `POST_BY` varchar(100) NOT NULL,
  `POST_DATE` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `noapara_bengal_visit_gallery`
--

INSERT INTO `noapara_bengal_visit_gallery` (`PICTURE_ID`, `PICTURE_TITLE`, `POST_BY`, `POST_DATE`) VALUES
(2, 'This is another  picture', 'demo', '2013-12-19'),
(4, 'fadsfa', 'demo', '2013-12-19');

-- --------------------------------------------------------

--
-- Table structure for table `noapara_cement_mills`
--

CREATE TABLE IF NOT EXISTS `noapara_cement_mills` (
`CEMENT_ID` int(100) NOT NULL,
  `CEMENT_DESCRIPTION` longtext NOT NULL,
  `POST_BY` varchar(100) NOT NULL,
  `POST_DATE` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `noapara_cement_mills`
--

INSERT INTO `noapara_cement_mills` (`CEMENT_ID`, `CEMENT_DESCRIPTION`, `POST_BY`, `POST_DATE`) VALUES
(1, 'Thils is jslfjsldlf;a<br>', 'imran', '2013-12-19');

-- --------------------------------------------------------

--
-- Table structure for table `noapara_cement_mills_picture`
--

CREATE TABLE IF NOT EXISTS `noapara_cement_mills_picture` (
`PICTURE_ID` int(50) NOT NULL,
  `PICTURE_TITLE` varchar(150) NOT NULL,
  `POST_BY` varchar(50) NOT NULL,
  `POST_DATE` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `noapara_cement_mills_picture`
--

INSERT INTO `noapara_cement_mills_picture` (`PICTURE_ID`, `PICTURE_TITLE`, `POST_BY`, `POST_DATE`) VALUES
(1, 'sdfgasd', 'imran', '2013-12-19');

-- --------------------------------------------------------

--
-- Table structure for table `noapara_concord_overview`
--

CREATE TABLE IF NOT EXISTS `noapara_concord_overview` (
`CONCORD_ID` int(50) NOT NULL,
  `CONCORD_OVERVIEW` longtext NOT NULL,
  `POST_BY` varchar(100) NOT NULL,
  `POST_DATE` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `noapara_concord_overview`
--

INSERT INTO `noapara_concord_overview` (`CONCORD_ID`, `CONCORD_OVERVIEW`, `POST_BY`, `POST_DATE`) VALUES
(1, 'THIS IS NWE SDJFLSAJDL;F fsdfsadfasdfasdfsadfasdf fas fs<br>', 'imran', '2013-12-19');

-- --------------------------------------------------------

--
-- Table structure for table `noapara_concord_photogallery`
--

CREATE TABLE IF NOT EXISTS `noapara_concord_photogallery` (
`PICTURE_ID` int(50) NOT NULL,
  `PICTURE_TITLE` varchar(200) NOT NULL,
  `POST_BY` varchar(100) NOT NULL,
  `POST_DATE` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `noapara_concord_photogallery`
--

INSERT INTO `noapara_concord_photogallery` (`PICTURE_ID`, `PICTURE_TITLE`, `POST_BY`, `POST_DATE`) VALUES
(2, 'dddd', 'demo', '2013-12-19');

-- --------------------------------------------------------

--
-- Table structure for table `noapara_concord_product_list`
--

CREATE TABLE IF NOT EXISTS `noapara_concord_product_list` (
`PRODUCT_ID` int(100) NOT NULL,
  `PRODUCT_NAME` varchar(150) NOT NULL,
  `GENERIC_NAME` varchar(150) NOT NULL,
  `POST_BY` varchar(150) NOT NULL,
  `POST_DATE` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `noapara_concord_product_list`
--

INSERT INTO `noapara_concord_product_list` (`PRODUCT_ID`, `PRODUCT_NAME`, `GENERIC_NAME`, `POST_BY`, `POST_DATE`) VALUES
(1, 'Acedol Tablet', 'Aceclofenac 100 mg', 'imran', '2013-12-17'),
(3, 'Acusan Plus Tablet', 'Losartan Potassium 50 mg+Hydrochlorathia', 'demo', '2013-12-17'),
(4, 'Acusan-50 Tablet', 'Losartan Potassium 50 mg+Hydrochlorathia', 'demo', '2013-12-17');

-- --------------------------------------------------------

--
-- Table structure for table `noapara_group_profile`
--

CREATE TABLE IF NOT EXISTS `noapara_group_profile` (
`PROFILE_ID` int(100) NOT NULL,
  `PROFILE_TITLE` varchar(100) NOT NULL,
  `DESCRIPTION` longtext NOT NULL,
  `POST_BY` varchar(50) NOT NULL,
  `POST_DATE` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `noapara_group_profile`
--

INSERT INTO `noapara_group_profile` (`PROFILE_ID`, `PROFILE_TITLE`, `DESCRIPTION`, `POST_BY`, `POST_DATE`) VALUES
(1, 'This is another title', 'gfjhgjhhvj<br>', 'imran', '2013-12-17');

-- --------------------------------------------------------

--
-- Table structure for table `noapara_home_bengal`
--

CREATE TABLE IF NOT EXISTS `noapara_home_bengal` (
`HOME_BENGAL_ID` int(50) NOT NULL,
  `PROFILE_TITLE` varchar(200) NOT NULL,
  `DESCRIPTION` longtext NOT NULL,
  `POST_BY` varchar(100) NOT NULL,
  `POST_DATE` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `noapara_home_bengal`
--

INSERT INTO `noapara_home_bengal` (`HOME_BENGAL_ID`, `PROFILE_TITLE`, `DESCRIPTION`, `POST_BY`, `POST_DATE`) VALUES
(1, 'This is another title', 'sdfsadgvddsf<br>', 'imran', '2013-12-19');

-- --------------------------------------------------------

--
-- Table structure for table `noapara_home_cement`
--

CREATE TABLE IF NOT EXISTS `noapara_home_cement` (
`HOME_CEMENT_ID` int(50) NOT NULL,
  `PROFILE_TITLE` varchar(200) NOT NULL,
  `DESCRIPTION` longtext NOT NULL,
  `POST_BY` varchar(100) NOT NULL,
  `POST_DATE` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `noapara_home_cement`
--

INSERT INTO `noapara_home_cement` (`HOME_CEMENT_ID`, `PROFILE_TITLE`, `DESCRIPTION`, `POST_BY`, `POST_DATE`) VALUES
(1, 'This is title o', 'kldjfsadjfl;askjd''f', 'imran', '2013-12-19');

-- --------------------------------------------------------

--
-- Table structure for table `noapara_home_concord`
--

CREATE TABLE IF NOT EXISTS `noapara_home_concord` (
`HOME_CONCORD_ID` int(50) NOT NULL,
  `PROFILE_TITLE` varchar(200) NOT NULL,
  `DESCRIPTION` longtext NOT NULL,
  `POST_BY` varchar(100) NOT NULL,
  `POST_DATE` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `noapara_home_concord`
--

INSERT INTO `noapara_home_concord` (`HOME_CONCORD_ID`, `PROFILE_TITLE`, `DESCRIPTION`, `POST_BY`, `POST_DATE`) VALUES
(1, 'This is title', 'sdflsdjfl;sdafasd', 'imran', '2013-12-19');

-- --------------------------------------------------------

--
-- Table structure for table `noapara_home_sister_concern`
--

CREATE TABLE IF NOT EXISTS `noapara_home_sister_concern` (
`SISTER_CONCERN_ID` int(100) NOT NULL,
  `PROFILE_TITLE` longtext NOT NULL,
  `POST_BY` varchar(100) NOT NULL,
  `POST_DATE` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `noapara_home_sister_concern`
--

INSERT INTO `noapara_home_sister_concern` (`SISTER_CONCERN_ID`, `PROFILE_TITLE`, `POST_BY`, `POST_DATE`) VALUES
(1, 'This is a picture', 'imran', '2013-12-19');

-- --------------------------------------------------------

--
-- Table structure for table `noapara_photo_gallery`
--

CREATE TABLE IF NOT EXISTS `noapara_photo_gallery` (
`PICTURE_ID` int(100) NOT NULL,
  `PICTURE_TITLE` varchar(100) NOT NULL,
  `POST_BY` varchar(100) NOT NULL,
  `POST_DATE` date NOT NULL,
  `PICTURE_ACTIVE` int(100) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `noapara_photo_gallery`
--

INSERT INTO `noapara_photo_gallery` (`PICTURE_ID`, `PICTURE_TITLE`, `POST_BY`, `POST_DATE`, `PICTURE_ACTIVE`) VALUES
(1, 'First ', 'imran', '2013-12-17', 1),
(2, 'This is a picture', 'demo', '2013-12-17', 1),
(3, 'This is another  picture', 'demo', '2013-12-17', 1),
(16, 'Fild', 'mahin', '2014-05-24', 1),
(17, 'Lad', 'mahin', '2014-05-24', 1),
(18, 'Factory', 'mahin', '2014-05-24', 1),
(19, 'Visit1', 'mahin', '2014-05-24', 1),
(20, 'Visit2', 'mahin', '2014-05-24', 1),
(21, 'Visit3', 'mahin', '2014-05-24', 1),
(22, 'Visit4', 'mahin', '2014-05-24', 1),
(23, 'Fild2', 'mahin', '2014-05-24', 1),
(24, 'Fild3', 'mahin', '2014-05-24', 1),
(25, 'Lad2', 'mahin', '2014-05-24', 1);

-- --------------------------------------------------------

--
-- Table structure for table `noapara_post`
--

CREATE TABLE IF NOT EXISTS `noapara_post` (
`POST_ID` int(50) NOT NULL,
  `TYPE` varchar(150) NOT NULL,
  `POST_TEXT` longtext NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `noapara_post`
--

INSERT INTO `noapara_post` (`POST_ID`, `TYPE`, `POST_TEXT`) VALUES
(1, 'VISION', 'Be the leader in marketing of all sorts of chemical fertilizer and manufacturing of mixed fertilizer &amp; agro based products and serves our most valuable end users.'),
(2, 'MISSION', 'Mission: Quality product through import and manufacturing with a view to maximize satisfaction of the ukimate user.'),
(3, 'GOAL', 'Goal: <br><ul><li>Priority in social responsibility.</li><li>To reach in hub of the farmers.</li></ul>'),
(4, 'OBJECTIVE', 'Objective: Quality product through import and manufacturing with a view to maximize satisfaction of the ukimate user.');

-- --------------------------------------------------------

--
-- Table structure for table `noapara_web_info`
--

CREATE TABLE IF NOT EXISTS `noapara_web_info` (
`WEBSITE_INFO_ID` int(100) NOT NULL,
  `WEBSITE_TITLE_NAME` varchar(100) NOT NULL,
  `WEBSITE_URL` text NOT NULL,
  `ADDRESS` varchar(100) NOT NULL,
  `CITY` varchar(100) NOT NULL,
  `POSTAL_CODE` int(100) NOT NULL,
  `PHONE_NUMBER` int(100) NOT NULL,
  `EMAIL` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `noapara_web_info`
--

INSERT INTO `noapara_web_info` (`WEBSITE_INFO_ID`, `WEBSITE_TITLE_NAME`, `WEBSITE_URL`, `ADDRESS`, `CITY`, `POSTAL_CODE`, `PHONE_NUMBER`, `EMAIL`) VALUES
(1, 'Noapara Group', 'www.noaparagroupbd.com', 'Mojijhil', 'Dhaka', 1205, 100000000, 'info@noaparagroupbd.com');

-- --------------------------------------------------------

--
-- Table structure for table `page_url`
--

CREATE TABLE IF NOT EXISTS `page_url` (
`page_id` int(10) unsigned NOT NULL,
  `page_name` varchar(200) NOT NULL,
  `page_url` varchar(200) NOT NULL,
  `module_id` int(10) unsigned NOT NULL,
  `fast_path` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page_url`
--

INSERT INTO `page_url` (`page_id`, `page_name`, `page_url`, `module_id`, `fast_path`) VALUES
(16, 'Website Information', 'webinfo.php', 1, 101),
(18, 'South Bengal(Home)', 'home_bengal.php', 1, 102),
(19, 'Objective', 'objective.php', 1, 305),
(20, 'Noapara Cement(Home)', 'home_cement.php', 1, 306),
(21, 'Concord Pharmaceuticals(Home)', 'home_concord.php', 1, 307),
(23, 'Slider', 'slider.php', 1, 103),
(24, 'Photo Gallery', 'picture.php', 3, 403),
(26, 'Chairman Message', 'chairman_message.php', 1, 405),
(27, 'MD Message', 'md_message.php', 1, 406),
(28, 'Group Profile', 'group_profile.php', 3, 407),
(29, 'Create a new Admin', 'adminsetup.php', 5, 501),
(30, 'Role Setup', 'role.php', 5, 502),
(32, 'User Management', 'user_man.php', 3, 701),
(34, 'Vision', 'vision.php', 1, 210),
(35, 'Mission', 'mission.php', 1, 208),
(36, 'Goal', 'goal.php', 1, 805),
(37, 'Welcome Text', 'home_welcome_text.php', 1, 110),
(38, 'Bengal Photogallery', 'bengal_photogallery.php', 2, 210),
(39, 'Bengal Quality Assurance', 'bengal_qa_photo.php', 2, 211),
(40, 'The Man', 'the_man.php', 2, 212),
(41, 'Product Marketing', 'bengal_product_marketing.php', 2, 213),
(42, 'Noapara Cement Overview', 'cement_mills.php', 3, 305),
(43, 'Noapara Cement Picture', 'cement_mills_picture.php', 3, 304),
(44, 'Concord Overview', 'concord_overview.php', 4, 205),
(45, 'Product  List', 'product_insert.php', 4, 408),
(46, 'Concord  Photogallery', 'concord_photogallery.php', 4, 404),
(47, 'Bengal Visit Photo', 'bengal_visit_gallery.php', 2, 213);

-- --------------------------------------------------------

--
-- Table structure for table `role_page`
--

CREATE TABLE IF NOT EXISTS `role_page` (
`role_page_id` int(10) unsigned NOT NULL,
  `page_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=120 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_page`
--

INSERT INTO `role_page` (`role_page_id`, `page_id`, `role_id`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 4, 1),
(5, 5, 1),
(6, 6, 1),
(7, 7, 1),
(8, 8, 1),
(9, 9, 1),
(10, 10, 1),
(11, 11, 1),
(12, 12, 1),
(13, 13, 1),
(14, 14, 1),
(16, 16, 1),
(18, 18, 1),
(19, 19, 1),
(20, 20, 1),
(21, 21, 1),
(22, 22, 1),
(23, 23, 1),
(24, 24, 1),
(25, 25, 1),
(26, 26, 1),
(27, 27, 1),
(28, 28, 1),
(29, 29, 1),
(30, 30, 1),
(31, 31, 1),
(32, 32, 1),
(33, 33, 1),
(34, 15, 2),
(35, 16, 2),
(36, 17, 2),
(37, 18, 2),
(38, 19, 2),
(39, 20, 2),
(40, 21, 2),
(41, 22, 2),
(42, 23, 2),
(43, 24, 2),
(44, 25, 2),
(45, 26, 2),
(46, 27, 2),
(47, 28, 2),
(48, 29, 2),
(49, 30, 2),
(50, 31, 2),
(51, 32, 2),
(52, 15, 3),
(53, 16, 3),
(54, 17, 3),
(55, 18, 3),
(56, 19, 3),
(57, 20, 3),
(58, 21, 3),
(59, 22, 3),
(60, 23, 3),
(61, 24, 3),
(62, 25, 3),
(63, 26, 3),
(64, 27, 3),
(65, 28, 3),
(66, 29, 3),
(67, 30, 3),
(68, 31, 3),
(69, 32, 3),
(70, 15, 4),
(71, 16, 4),
(72, 17, 4),
(73, 18, 4),
(74, 19, 4),
(75, 20, 4),
(76, 21, 4),
(77, 22, 4),
(78, 23, 4),
(79, 24, 4),
(80, 25, 4),
(81, 26, 4),
(82, 27, 4),
(83, 28, 4),
(84, 29, 4),
(85, 30, 4),
(86, 31, 4),
(87, 32, 4),
(88, 16, 5),
(89, 18, 5),
(90, 19, 5),
(91, 20, 5),
(92, 21, 5),
(93, 22, 5),
(94, 23, 5),
(95, 24, 5),
(96, 25, 5),
(97, 26, 5),
(98, 27, 5),
(99, 28, 5),
(100, 29, 5),
(101, 30, 5),
(102, 31, 5),
(103, 32, 5),
(104, 34, 1),
(106, 35, 2),
(107, 35, 1),
(108, 36, 1),
(109, 37, 1),
(110, 38, 1),
(111, 39, 1),
(112, 40, 1),
(113, 41, 1),
(114, 42, 1),
(115, 43, 1),
(116, 44, 1),
(117, 45, 1),
(118, 46, 1),
(119, 47, 1);

-- --------------------------------------------------------

--
-- Table structure for table `role_previlege`
--

CREATE TABLE IF NOT EXISTS `role_previlege` (
`preevilige_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `p_add` int(10) unsigned NOT NULL,
  `p_delete` int(10) unsigned NOT NULL,
  `p_edit` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_previlege`
--

INSERT INTO `role_previlege` (`preevilige_id`, `role_id`, `p_add`, `p_delete`, `p_edit`) VALUES
(1, 1, 1, 1, 1),
(2, 2, 1, 0, 0),
(3, 3, 0, 1, 0),
(4, 4, 0, 0, 1),
(5, 5, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `role_setup`
--

CREATE TABLE IF NOT EXISTS `role_setup` (
`role_id` int(10) unsigned NOT NULL,
  `role_name` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_setup`
--

INSERT INTO `role_setup` (`role_id`, `role_name`) VALUES
(1, 'Super Admin'),
(2, 'only add'),
(3, 'delete'),
(4, 'edit'),
(5, 'Visitor');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE IF NOT EXISTS `tbl_admin` (
`admin_id` int(3) NOT NULL,
  `admin_name` varchar(50) DEFAULT NULL,
  `admin_email_address` varchar(100) DEFAULT NULL,
  `admin_password` varchar(30) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`admin_id`, `admin_name`, `admin_email_address`, `admin_password`) VALUES
(1, 'dynamic', 'dynamicsoft@gmail.com', '1234');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_chairman_message`
--

CREATE TABLE IF NOT EXISTS `tbl_chairman_message` (
`message_id` int(3) NOT NULL,
  `chairman_name` varchar(100) NOT NULL,
  `message_short_list` varchar(500) NOT NULL,
  `message_list` varchar(3000) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_chairman_message`
--

INSERT INTO `tbl_chairman_message` (`message_id`, `chairman_name`, `message_short_list`, `message_list`, `image`) VALUES
(1, 'Mr. Md. Faizur Rahman Bokul', 'It is an immense pleasure to express my gratitude to our valued customers and patrons. I never fail to thank to\r\n           those who are in from our dewdrops of water to till date to form a group named "Noapara Group".', 'It is an immense pleasure to express my gratitude to our valued customers and patrons. I never fail to thank to\r\n           those who are in from our dewdrops of water to till date to form a group named "Noapara Group". Our business is\r\n            now diversified from import to manufacture. "Noapara Group" is now manufactured quality cement and latest art of\r\n           technology in fertilizer sector is NPKS. Our main motto is to help to the development of agriculture sector.</p>\r\n          <p>Bangladesh is dense populated area but their facilities in good seeds, crops keeping processes, good \r\n          fertilizer availability is not enough Our Group istrying heart and soul to meet the above mentioned things.</p>\r\n        <p>We believe that our two products- quality cement& lasted quality fertilizer NPKS will fulfill the man''stwo basic\r\n         demand Besides we are importing food grain & others important fertilizer.</p>\r\n        <p>We are always for the people and only think to impart in country development. Lastly we thank our clients and\r\n         patrons for their continuous support with our quality products & Services.</p>\r\n        <p>In Bangladesh Pharmaceutical sector is the core of healthcare sector which is contributing the second largest\r\n         revenue to the country''s economy. "CONCORD Pharmaceuticals Ltd." is determined to manufacture high quality health\r\n          care products that improve the quality of life of people.</p>\r\n        <p>We belief in "Better Medicine Better life" which contributes towards the growth of a healthy Nation.', 'images/chairman.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_md_message`
--

CREATE TABLE IF NOT EXISTS `tbl_md_message` (
`message_id` int(3) NOT NULL,
  `md_name` varchar(100) NOT NULL,
  `message_short_list` varchar(500) NOT NULL,
  `message_list` varchar(3000) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_md_message`
--

INSERT INTO `tbl_md_message` (`message_id`, `md_name`, `message_short_list`, `message_list`, `image`) VALUES
(1, 'Mr. Md. Saidur Rahman Litu ', 'Noapara Group" is formed with a team of quality manpower. Every sector is going on quality basis .Though the company starts its business with trading but within 20 years it expands on', 'Noapara Group" is formed with a team of quality manpower. Every sector is going on quality basis .Though the\r\n           company starts its business with trading but within 20 years it expands on imported and man ufacturing.The\r\n           Concern follows "ISO 9001:2000" quality management system for the betterment of our products</p>\r\n          <p>"Noapara Group" always believes in quality and reliability. With the Iicense of BSTI we are producing quality\r\n          cement and delivering it in every corner of our country. By our product construction will be safety and moneywill\r\n          be saved.</p>\r\n         <p>In dense populated area more crops producing is essential. To produce the same you must have to use good\r\n         fertilizer. We are announcing that we produ do g lasted art of fertilizer na rued NPKS with different Gra de and\r\n         also imported another quality fertilizer through quality checking. Not only we but also every country in the world\r\n         is produdng balanced fertilizer named NPKS. We always believe that no body will deceive from our products. So we\r\n         believe that....................</p>\r\n        <p>Useourgood products grow more crops and such one can impart to country darelopment through lesscost in crop\r\n        producing.</p>\r\n        <p>The Pharmaceutical sector in Bangladesh provides 97% of the total medicine requirement of the local market. Being\r\n        part of healthcare sector, its performance is related to demographic variables like population growth as well as\r\n        economic growth and healthcare policy.</p>\r\n        <p>"CONCORD Pharmaceuticals Ltd." the fast growing company is committed to manufacturing of quality medicines\r\n        through strict adherence to internationally accepted standards of Good Manufacturing Practice.\r\n"CONCORD Pharmaceuticals Ltd." the fast growing company is committed to manufacturing of quality medicines through strict adherence to internationally accepted standards of Good Manufacturing Practice.', 'images/MD.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_slider`
--

CREATE TABLE IF NOT EXISTS `tbl_slider` (
`slider_id` int(3) NOT NULL,
  `slider_image` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_slider`
--

INSERT INTO `tbl_slider` (`slider_id`, `slider_image`) VALUES
(1, 'images/slider/1.jpg'),
(3, 'images/slider/2.jpg'),
(4, 'images/slider/3.jpg'),
(5, 'images/slider/4.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_welcome_message`
--

CREATE TABLE IF NOT EXISTS `tbl_welcome_message` (
`welcome_id` int(3) NOT NULL,
  `welcome_short_message` varchar(100) DEFAULT NULL,
  `welcome_long_message` varchar(500) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_welcome_message`
--

INSERT INTO `tbl_welcome_message` (`welcome_id`, `welcome_short_message`, `welcome_long_message`) VALUES
(1, 'WELCOME TO NOAPARA GROUP ', ' NOAPARA GROUP BECOMING BANGLADESH''S LEADING  FERTILIZER COMPANY  ');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `menu_module`
--
ALTER TABLE `menu_module`
 ADD PRIMARY KEY (`module_id`);

--
-- Indexes for table `noapara_bengal_photogallery`
--
ALTER TABLE `noapara_bengal_photogallery`
 ADD PRIMARY KEY (`PICTURE_ID`);

--
-- Indexes for table `noapara_bengal_product_marketing`
--
ALTER TABLE `noapara_bengal_product_marketing`
 ADD PRIMARY KEY (`PRODUCT_ID`);

--
-- Indexes for table `noapara_bengal_qa_photogallery`
--
ALTER TABLE `noapara_bengal_qa_photogallery`
 ADD PRIMARY KEY (`QA_ID`);

--
-- Indexes for table `noapara_bengal_the_man`
--
ALTER TABLE `noapara_bengal_the_man`
 ADD PRIMARY KEY (`THE_MAN_ID`);

--
-- Indexes for table `noapara_bengal_visit_gallery`
--
ALTER TABLE `noapara_bengal_visit_gallery`
 ADD PRIMARY KEY (`PICTURE_ID`);

--
-- Indexes for table `noapara_cement_mills`
--
ALTER TABLE `noapara_cement_mills`
 ADD PRIMARY KEY (`CEMENT_ID`);

--
-- Indexes for table `noapara_cement_mills_picture`
--
ALTER TABLE `noapara_cement_mills_picture`
 ADD PRIMARY KEY (`PICTURE_ID`);

--
-- Indexes for table `noapara_concord_overview`
--
ALTER TABLE `noapara_concord_overview`
 ADD PRIMARY KEY (`CONCORD_ID`);

--
-- Indexes for table `noapara_concord_photogallery`
--
ALTER TABLE `noapara_concord_photogallery`
 ADD PRIMARY KEY (`PICTURE_ID`);

--
-- Indexes for table `noapara_concord_product_list`
--
ALTER TABLE `noapara_concord_product_list`
 ADD PRIMARY KEY (`PRODUCT_ID`);

--
-- Indexes for table `noapara_group_profile`
--
ALTER TABLE `noapara_group_profile`
 ADD PRIMARY KEY (`PROFILE_ID`);

--
-- Indexes for table `noapara_home_bengal`
--
ALTER TABLE `noapara_home_bengal`
 ADD PRIMARY KEY (`HOME_BENGAL_ID`);

--
-- Indexes for table `noapara_home_cement`
--
ALTER TABLE `noapara_home_cement`
 ADD PRIMARY KEY (`HOME_CEMENT_ID`);

--
-- Indexes for table `noapara_home_concord`
--
ALTER TABLE `noapara_home_concord`
 ADD PRIMARY KEY (`HOME_CONCORD_ID`);

--
-- Indexes for table `noapara_home_sister_concern`
--
ALTER TABLE `noapara_home_sister_concern`
 ADD PRIMARY KEY (`SISTER_CONCERN_ID`);

--
-- Indexes for table `noapara_photo_gallery`
--
ALTER TABLE `noapara_photo_gallery`
 ADD PRIMARY KEY (`PICTURE_ID`);

--
-- Indexes for table `noapara_post`
--
ALTER TABLE `noapara_post`
 ADD PRIMARY KEY (`POST_ID`);

--
-- Indexes for table `noapara_web_info`
--
ALTER TABLE `noapara_web_info`
 ADD PRIMARY KEY (`WEBSITE_INFO_ID`);

--
-- Indexes for table `page_url`
--
ALTER TABLE `page_url`
 ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `role_page`
--
ALTER TABLE `role_page`
 ADD PRIMARY KEY (`role_page_id`);

--
-- Indexes for table `role_previlege`
--
ALTER TABLE `role_previlege`
 ADD PRIMARY KEY (`preevilige_id`);

--
-- Indexes for table `role_setup`
--
ALTER TABLE `role_setup`
 ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
 ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `tbl_chairman_message`
--
ALTER TABLE `tbl_chairman_message`
 ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `tbl_md_message`
--
ALTER TABLE `tbl_md_message`
 ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `tbl_slider`
--
ALTER TABLE `tbl_slider`
 ADD PRIMARY KEY (`slider_id`);

--
-- Indexes for table `tbl_welcome_message`
--
ALTER TABLE `tbl_welcome_message`
 ADD PRIMARY KEY (`welcome_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `menu_module`
--
ALTER TABLE `menu_module`
MODIFY `module_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `noapara_bengal_photogallery`
--
ALTER TABLE `noapara_bengal_photogallery`
MODIFY `PICTURE_ID` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `noapara_bengal_product_marketing`
--
ALTER TABLE `noapara_bengal_product_marketing`
MODIFY `PRODUCT_ID` int(50) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `noapara_bengal_qa_photogallery`
--
ALTER TABLE `noapara_bengal_qa_photogallery`
MODIFY `QA_ID` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `noapara_bengal_the_man`
--
ALTER TABLE `noapara_bengal_the_man`
MODIFY `THE_MAN_ID` int(50) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `noapara_bengal_visit_gallery`
--
ALTER TABLE `noapara_bengal_visit_gallery`
MODIFY `PICTURE_ID` int(50) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `noapara_cement_mills`
--
ALTER TABLE `noapara_cement_mills`
MODIFY `CEMENT_ID` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `noapara_cement_mills_picture`
--
ALTER TABLE `noapara_cement_mills_picture`
MODIFY `PICTURE_ID` int(50) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `noapara_concord_overview`
--
ALTER TABLE `noapara_concord_overview`
MODIFY `CONCORD_ID` int(50) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `noapara_concord_photogallery`
--
ALTER TABLE `noapara_concord_photogallery`
MODIFY `PICTURE_ID` int(50) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `noapara_concord_product_list`
--
ALTER TABLE `noapara_concord_product_list`
MODIFY `PRODUCT_ID` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `noapara_group_profile`
--
ALTER TABLE `noapara_group_profile`
MODIFY `PROFILE_ID` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `noapara_home_bengal`
--
ALTER TABLE `noapara_home_bengal`
MODIFY `HOME_BENGAL_ID` int(50) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `noapara_home_cement`
--
ALTER TABLE `noapara_home_cement`
MODIFY `HOME_CEMENT_ID` int(50) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `noapara_home_concord`
--
ALTER TABLE `noapara_home_concord`
MODIFY `HOME_CONCORD_ID` int(50) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `noapara_home_sister_concern`
--
ALTER TABLE `noapara_home_sister_concern`
MODIFY `SISTER_CONCERN_ID` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `noapara_photo_gallery`
--
ALTER TABLE `noapara_photo_gallery`
MODIFY `PICTURE_ID` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `noapara_post`
--
ALTER TABLE `noapara_post`
MODIFY `POST_ID` int(50) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `noapara_web_info`
--
ALTER TABLE `noapara_web_info`
MODIFY `WEBSITE_INFO_ID` int(100) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `page_url`
--
ALTER TABLE `page_url`
MODIFY `page_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `role_page`
--
ALTER TABLE `role_page`
MODIFY `role_page_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=120;
--
-- AUTO_INCREMENT for table `role_previlege`
--
ALTER TABLE `role_previlege`
MODIFY `preevilige_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `role_setup`
--
ALTER TABLE `role_setup`
MODIFY `role_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
MODIFY `admin_id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_chairman_message`
--
ALTER TABLE `tbl_chairman_message`
MODIFY `message_id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_md_message`
--
ALTER TABLE `tbl_md_message`
MODIFY `message_id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_slider`
--
ALTER TABLE `tbl_slider`
MODIFY `slider_id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_welcome_message`
--
ALTER TABLE `tbl_welcome_message`
MODIFY `welcome_id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
